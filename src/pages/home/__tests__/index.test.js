import React from 'react';
import withJquery  from '../index';
import renderer from 'react-test-renderer';

it('Content  testing', () => {
    const tree = renderer.create(<withJquery  />).toJSON();
    expect(tree).toMatchSnapshot();
});