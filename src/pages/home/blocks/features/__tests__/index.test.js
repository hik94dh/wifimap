import React from 'react';
import Features from '../index';
import renderer from 'react-test-renderer';

it('Features testing', () => {
    const tree = renderer.create(<Features />).toJSON();
    expect(tree).toMatchSnapshot();
});