import React from 'react';

export default () => (
  <div>
    <div id="features" />
    <div
      data-0="@data-fixed: false; @data-act1: false; @data-act2: false; @data-act3: false; @data-act4: false;"
      data-0-top-top="@data-fixed: true; @data-act1: true;"
      data--800-top-top="@data-act1: false; @data-act2: true;"
      data--1600-top-top="@data-act1: false; @data-act2: false; @data-act3: true;"
      data--2400-top-top="@data-act1: false; @data-act2: false; @data-act3: false; @data-act4: true;"
      data--3200-top-top="@data-fixed: scrollout;"
      data-anchor-target="#features"
      className="features"
    >
      <div className="features__container">
        <div className="features__content">
          <div className="wrap">
            <div className="parts-row-table parts-2 parts-collapse-lg">
              <div className="col-item">
                <div className="features__image hdn-lg">
                  <img src="/img/f-img1.png" alt="" className="f-img1" />
                  <img src="/img/f-img2.png" alt="" className="f-img2" />
                  <img src="/img/f-img3.png" alt="" className="f-img3" />
                  <img src="/img/f-img4.png" alt="" className="f-img4" />
                </div>
              </div>
              <div className="col-item">
                <div className="features__descr">
                  <div className="features__title">Features & Functions</div>
                  <ul className="features-list">
                    <li className="features-list__item f-item1">
                      <div className="ic-bg ic-feat1" />
                      <div className="features-list__title">
                        100 Million Free WiFis
                      </div>
                      <div className="features-list__text">
                        Over 100'000'000 accessible Wi-Fi hotspot locations
                      </div>
                    </li>
                    <li className="features-list__item f-item2">
                      <div className="ic-bg ic-feat2" />
                      <div className="features-list__title">
                        Passwords & Comments
                      </div>
                      <div className="features-list__text">
                        Map and List Views with passwords & comments for Wi-Fi
                      </div>
                    </li>
                    <li className="features-list__item f-item3">
                      <div className="ic-bg ic-feat3" />
                      <div className="features-list__title">
                        Social Community
                      </div>
                      <div className="features-list__text">
                        All Wi-Fi hotspots are added by users. You can do it,
                        its easy.
                      </div>
                    </li>
                    <li className="features-list__item f-item4">
                      <div className="ic-bg ic-feat4" />
                      <div className="features-list__title">
                        Worldwide Free Offline Cities
                      </div>
                      <div className="features-list__text">
                        America, Europe, Asia, Australia, Middle East, Russia,
                        Africa
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);
