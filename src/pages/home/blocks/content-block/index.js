import React from 'react';

export default () => (
  <div className="content-block">
    <div className="section">
      <div className="wrap">
        <div className="sub-promo">
          <div className="parts-row-table parts-2 parts-divide-lg parts-collapse-lg">
            <div className="col-item">
              <div
                data-offset="250"
                className="faces-screen anim-container hdn-lg"
              >
                <img
                  src="img/faces-scheme.png"
                  srcSet="img/faces-scheme@2x.png 2x"
                  alt=""
                  className="faces-screen__scheme anim-bg"
                />
                <div className="faces-screen__items">
                  <img
                    src="img/face1.png"
                    alt=""
                    srcSet="img/face1@2x.png 2x"
                    className="faces-screen__item faces-screen__item--face1 anim-image anim-image-1"
                  />
                  <img
                    src="img/face2.png"
                    alt=""
                    srcSet="img/face2@2x.png 2x"
                    className="faces-screen__item faces-screen__item--face2 anim-image anim-image-2"
                  />
                  <img
                    src="img/face3.png"
                    alt=""
                    srcSet="img/face3@2x.png 2x"
                    className="faces-screen__item faces-screen__item--face3 anim-image anim-image-4"
                  />
                  <img
                    src="img/face4.png"
                    alt=""
                    srcSet="img/face4@2x.png 2x"
                    className="faces-screen__item faces-screen__item--face4 anim-image anim-image-7"
                  />
                  <img
                    src="img/face5.png"
                    alt=""
                    srcSet="img/face5@2x.png 2x"
                    className="faces-screen__item faces-screen__item--face5 anim-image anim-image-8"
                  />
                  <img
                    src="img/face6.png"
                    alt=""
                    srcSet="img/face6@2x.png 2x"
                    className="faces-screen__item faces-screen__item--face6 anim-image anim-image-9"
                  />
                  <img
                    src="img/face7.png"
                    alt=""
                    srcSet="img/face7@2x.png 2x"
                    className="faces-screen__item faces-screen__item--face7 anim-image anim-image-5"
                  />
                  <img
                    src="img/face8.png"
                    alt=""
                    srcSet="img/face8@2x.png 2x"
                    className="faces-screen__item faces-screen__item--face8 anim-image anim-image-4"
                  />
                  <img
                    src="img/face9.png"
                    alt=""
                    srcSet="img/face9@2x.png 2x"
                    className="faces-screen__item faces-screen__item--face9 anim-image anim-image-3"
                  />
                </div>
                <img
                  src="img/device3.png"
                  srcSet="img/device3@2x.png 2x"
                  alt=""
                  className="faces-screen__device anim-center"
                />
              </div>
              <div
                style={{ backgroundImage: "url('img/people.png')" }}
                className="sub-promo__mob-screen show-lg"
              />
            </div>
            <div className="col-item">
              <div className="parts-row parts-5 ta-c parts-collapse-lg">
                <div className="col-item part-5x4 part-offset-1">
                  <div
                    data-bottom-top="opacity: 0; transform: translate3d(0,80px,0);"
                    data-80p-top-bottom="opacity: 1; transform: translate3d(0,0px,0);"
                    className="sub-promo__descr"
                  >
                    <div className="sub-promo__title">
                      Crowdsourced by real people
                    </div>
                    <div className="sub-promo__text">
                      <p>
                        WiFis are added by members of our community with a
                        single intent of helping others with Free internet
                        connectivity. You can also add new WiFis and share the
                        app with friends. When we are together we are
                        unstoppable. This is the power of sharing economy!
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="info-panel">
          <div className="parts-row parts-2 parts-collapse-lg">
            <div className="col-item">
              <div className="info-panel__desc info-panel__desc--left info-panel__desc--gray">
                <div className="info-panel__title">
                  Save Money - Use <span>Free WiFi</span>
                </div>
                <div className="info-panel__text">
                  <p>
                    Connect for FREE to more than 100 million WiFis worldwide.
                    Avoid roaming and data charges during your business trips,
                    family travels and EVERY DAY of your life. Millions of
                    people use our services on daily basis, download our app now
                    and enjoy the wonderful world of Free WiFi.
                  </p>
                </div>
              </div>
            </div>
            <div className="col-item">
              <div
                data-bottom-top="transform:translate3d(0px,80px,0px);"
                data-top-bottom="transform:translate3d(0px,-80px,0px);"
                className="info-panel__image info-panel__image--right skrollable skrollable-before"
                // style="transform: translate3d(0px, 80px, 0px);"
              >
                <img
                  src="/img/float-img1.jpg"
                  srcSet="/img/float-img1@2x.jpg 2x"
                  alt=""
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);
