import React from 'react';
import ContentBlock from '../index';
import renderer from 'react-test-renderer';

it('content-block testing', () => {
    const tree = renderer.create(<ContentBlock />).toJSON();
    expect(tree).toMatchSnapshot();
});