import React from 'react';
import OwlCarousel from 'react-owl-carousel';
import { Link } from 'react-router-dom';
import { graphql } from 'react-apollo';
import fetchTopCities from '@queries/fetchTopCities';
import splitNumbers from '@utils/splitNumbers';
import { number, shape, string } from "prop-types";

const Content = ({ data: { top_cities } }) => {
  const settings = {
    items: 4,
    dots: false,
    nav: true,
    margin: 20,
    smartSpeed: 500,
    loop: true,
    responsive: {
      0: {
        items: 1,
      },
      500: {
        items: 2,
      },
      1000: {
        items: 4,
      },
    },
  };

  const renderTopCities =
    top_cities &&
    top_cities.map(({ hotspots_count, name, slug }) => (
      <Link
        className="places-list__item places-list__item--center"
        key={slug}
        to={`/cities/${slug}`}
      >
        <span className="places-list__image" />
        <h3 className="places-list__title">{name}</h3>
        <span className="places-list__note">{splitNumbers(hotspots_count, ' ')} Free Wifi</span>
      </Link>
    ));
  return (
    <div className="content-block">
      <section className="section section--gray">
        <div className="wrap">
          <div className="section-title">
            <h2 className="section-title__title">Discover our Top Cities</h2>
          </div>
        </div>
        <div className="places-list">
          <div className="wrap">
            {top_cities && (
              <OwlCarousel className="places-list__slider" {...settings}>
                {renderTopCities}
              </OwlCarousel>
            )}

            <div className="ta-c">
              <Link
                className="btn btn-fill btn-lg ic-r ic-arr-r-sm-wh"
                to="/countries"
              >
                See all Countries
              </Link>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default graphql(fetchTopCities)(Content);

Content.propTypes = {
    top_cities: shape({
        hotspots_count: number,
        name: string,
        slug: string,
    }),
};
