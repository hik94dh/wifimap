import React from 'react';
import Promo from '../index';
import renderer from 'react-test-renderer';

it('Promo testing', () => {
    const tree = renderer.create(<Promo />).toJSON();
    expect(tree).toMatchSnapshot();
});