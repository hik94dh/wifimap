import React from 'react';

export default () => (
  <section className="main-promo">
    <div
      data-bottom-top="transform:translate3d(0px, -200px, 0px);"
      data-top-bottom="transform:translate3d(0px, 150px, 0px);"
      data-anchor-target=".main-promo"
      className="main-promo__parallax-bg js-parallax-bg hdn-lg"
    >
      <div
        style={{ backgroundImage: "url('img/promo-bg.jpg')" }}
        data-bottom-top="transform:translate3d(0px, 0, 0px);"
        data-top-bottom="transform:translate3d(0px, 0, 0px);"
        data-anchor-target=".main-promo"
        className="main-promo__parallax-bg-image"
      >
        <div
          data-offset="10"
          data-animate-anchor="#animate-anchor"
          data-bottom-top="transform:translate3d(0px, -140px, 0px);"
          data-top-bottom="transform:translate3d(0px, 0px, 0px);"
          data-anchor-target=".main-promo"
          className="placeholders anim-anchor"
        >
          <div className="placeholders__item placeholders__item--p1">
            <img
              src="/img/placeholder.png"
              srcSet="/img/placeholder@2x.png 2x"
              alt=""
            />
          </div>
          <div className="placeholders__item placeholders__item--p2">
            <img
              src="/img/placeholder.png"
              srcSet="/img/placeholder@2x.png 2x"
              alt=""
            />
          </div>
          <div className="placeholders__item placeholders__item--p3">
            <img
              src="/img/placeholder.png"
              srcSet="/img/placeholder@2x.png 2x"
              alt=""
            />
          </div>
          <div className="placeholders__item placeholders__item--p4">
            <img
              src="/img/placeholder.png"
              srcSet="/img/placeholder@2x.png 2x"
              alt=""
            />
          </div>
          <div className="placeholders__item placeholders__item--p5">
            <img
              src="/img/placeholder.png"
              srcSet="/img/placeholder@2x.png 2x"
              alt=""
            />
          </div>
          <div className="placeholders__item placeholders__item--p6">
            <img
              src="/img/placeholder.png"
              srcSet="/img/placeholder@2x.png 2x"
              alt=""
            />
          </div>
          <div className="placeholders__item placeholders__item--p7">
            <img
              src="/img/placeholder.png"
              srcSet="/img/placeholder@2x.png 2x"
              alt=""
            />
          </div>
          <div className="placeholders__item placeholders__item--p8">
            <img
              src="/img/placeholder.png"
              srcSet="/img/placeholder@2x.png 2x"
              alt=""
            />
          </div>
          <div className="placeholders__item placeholders__item--p9">
            <img
              src="/img/placeholder.png"
              srcSet="/img/placeholder@2x.png 2x"
              alt=""
            />
          </div>
          <div className="placeholders__item placeholders__item--p10">
            <img
              src="/img/placeholder.png"
              srcSet="/img/placeholder@2x.png 2x"
              alt=""
            />
          </div>
          <div className="placeholders__item placeholders__item--p11">
            <img
              src="/img/placeholder.png"
              srcSet="/img/placeholder@2x.png 2x"
              alt=""
            />
          </div>
          <div className="placeholders__item placeholders__item--p12">
            <img
              src="/img/placeholder.png"
              srcSet="/img/placeholder@2x.png 2x"
              alt=""
            />
          </div>
          <div className="placeholders__item placeholders__item--p13">
            <img
              src="/img/placeholder.png"
              srcSet="/img/placeholder@2x.png 2x"
              alt=""
            />
          </div>
          <div className="placeholders__item placeholders__item--p14">
            <img
              src="/img/placeholder.png"
              srcSet="/img/placeholder@2x.png 2x"
              alt=""
            />
          </div>
          <div className="placeholders__item placeholders__item--p15">
            <img
              src="/img/placeholder.png"
              srcSet="/img/placeholder@2x.png 2x"
              alt=""
            />
          </div>
          <div className="placeholders__item placeholders__item--p16">
            <img
              src="/img/placeholder.png"
              srcSet="/img/placeholder@2x.png 2x"
              alt=""
            />
          </div>
          <div className="placeholders__item placeholders__item--p17">
            <img
              src="/img/placeholder.png"
              srcSet="/img/placeholder@2x.png 2x"
              alt=""
            />
          </div>
          <div className="placeholders__item placeholders__item--p18">
            <img
              src="/img/placeholder.png"
              srcSet="/img/placeholder@2x.png 2x"
              alt=""
            />
          </div>
        </div>
      </div>
    </div>
    <div
      data-bottom-top="opacity: 0.8;"
      data-0-top-top="opacity: 0;"
      data-anchor-target=".main-promo"
      className="main-promo__parallax-bg-overlay"
    />
    <div
      style={{ backgroundImage: "url('img/promo-bg-mob.jpg')" }}
      className="main-promo__bg show-lg"
    />
    <img
      src="/img/promo-layer1.png"
      srcSet="/img/promo-layer1@2x.png 2x"
      alt=""
      data-bottom-top="transform:translate3d(-50px,-50px, 0px);"
      data-top-bottom="transform:translate3d(0px, 250px, 0px);"
      className="main-promo__layer main-promo__layer--l1 skrollable skrollable-between"
    />
    <div className="wrap">
      <div className="wrap-sm">
        <h1 className="main-promo__title">
          WiFi Map - Connecting the World one network at a time
        </h1>
        <div className="main-promo__text">
          Our users have added millions of WiFis in world’s busiest cities,
          airports, small towns and villages as well as the most remote areas of
          our wonderful planet. We want to give access to this basic right to
          anyone in the world!
        </div>
      </div>
      <div className="main-promo__image">
        <img
          src="/img/promo-layer2.png"
          srcSet="/img/promo-layer2@2x.png 2x"
          alt=""
          data-bottom-top="transform:translate3d(0,0px, 0px);"
          data-top-bottom="transform:translate3d(0px, 200px, 0px);"
          id="animate-anchor"
          className="main-promo__layer main-promo__layer--l2 skrollable skrollable-between"
        />
      </div>
    </div>
  </section>
);
