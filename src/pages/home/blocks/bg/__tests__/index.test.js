import React from 'react';
import Bg from '../index';
import renderer from 'react-test-renderer';

it('bg testing', () => {
    const tree = renderer.create(<Bg />).toJSON();
    expect(tree).toMatchSnapshot();
});