import React from 'react';

export default () => (
  <section className="bg-section">
    <div className="bg-section__content">
      <div className="bg-section__inner">
        <div
          data-bottom-top="transform:translate3d(0px, -10%, 0px);"
          data-top-bottom="transform:translate3d(0px, 10%, 0px);"
          data-anchor-target=".bg-section"
          className="bg-section__parallax-bg js-parallax-bg hdn-lg"
        >
          <div
            style={{ backgroundImage: "url('img/parallax-bg.jpg')" }}
            data-bottom-top="transform:translate3d(0px, -10%, 0px);"
            data-top-bottom="transform:translate3d(0px, 10%, 0px);"
            data-anchor-target=".bg-section"
            className="bg-section__parallax-bg-image"
          />
        </div>

        <div
          style={{ backgroundImage: "url('img/parallax-bg.jpg')" }}
          className="bg-section__bg show-lg"
        />
      </div>
    </div>
  </section>
);
