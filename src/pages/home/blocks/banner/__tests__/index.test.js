import React from 'react';
import Banner from '../index';
import renderer from 'react-test-renderer';
import { StaticRouter } from 'react-router';
import { ApolloProvider } from 'react-apollo';
import apolloClient from '../../../../../apollo-client';

it('banner testing', () => {
    const context = {};
    const tree = renderer
        .create(
            <StaticRouter context={context}>
                <ApolloProvider client={apolloClient}>
                    <Banner />
                </ApolloProvider>
            </StaticRouter>,
        )
        .toJSON();
    expect(tree).toMatchSnapshot();
});
