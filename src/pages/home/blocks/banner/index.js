import React from 'react';
import Search from '@components/google-places-search';
import './styles.scss';

export default () => (
  <div className="promo">
    <div className="promo__container">
      <div className="promo__content">
        <div className="promo__title load-anim-el animate">
          Free WiFi, Every day, Everywhere
        </div>
        <div className="promo__desc load-anim-el animate">
          <div>
            Imagine if someone had access to over{' '}
            <strong>100 Million Free WiFis.</strong>
          </div>
          You can become that person in 30 seconds by downloading our app and
          enjoying the wonderful world of FREE WiFi.
        </div>
      </div>
        <Search className="promo__search" />
    </div>
    <div className="tooltip">
      <div className="tooltip__inner load-anim-el">
        <div className="tooltip-ribbon">
          <div className="tooltip-ribbon__inner">
            <span className="ic-r ic-wifi">Live</span>
          </div>
        </div>
        <div className="tooltip__title">Get access to FREE WIFI worldwide</div>
        <div className="tooltip__numbers">
          {' '}
          <span data-num="100" className="num-el">
            100
          </span>,{' '}
          <span data-num="543" className="num-el">
            543
          </span>,{' '}
          <span data-num="984" className="num-el">
            984
          </span>
        </div>
      </div>
    </div>
  </div>
);
