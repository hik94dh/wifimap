import React from 'react';
import Banner from './blocks/banner';
import Promo from './blocks/main-promo';
import ContentBlock from './blocks/content-block';
import Features from './blocks/features';
import Bg from './blocks/bg';
import Share from '@components/share';
import SliderCities from './blocks/slider-cities';

import withJquery from './withJquery';

const Content = () => (
  <div className="page">
    <Banner />
    <Promo />
    <SliderCities />
    <ContentBlock />
    <Features />
    <Bg />
    <Share />
  </div>
);

export default withJquery(Content);
