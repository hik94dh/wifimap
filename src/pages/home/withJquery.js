import React, { Component } from 'react';
import $ from 'jquery';
// import 'owl.carousel/dist/assets/owl.carousel.css';
// eslint-disable-next-line
import skrollr from 'skrollr';

/* eslint-disable */
export default function withJquery(WrappedComponent) {
  return class extends Component {
    componentDidMount() {
      var click = 'click';
      var isMobile = {
        Android: function() {
          return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
          return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
          return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
          return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
          return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
          return (
            isMobile.Android() ||
            isMobile.BlackBerry() ||
            isMobile.iOS() ||
            isMobile.Opera() ||
            isMobile.Windows()
          );
        },
      };

      function loadAnimation() {
        var element = $('.load-anim-el');
        element.addClass('animate');
      }

      function numAnim() {
        var $numEl = $('.num-el');
        $numEl.each(function() {
          var $this = $(this);
          $({ Counter: 0 }).animate(
            { Counter: $this.data('num') },
            {
              duration: 2500,
              easing: 'linear',
              step: function() {
                $this.text(Math.ceil(this.Counter));
              },
            },
          );
        });
      }

      // parallax height
      function parallaxHeight() {
        var bgItem = $('.js-parallax-image');
        bgItem.each(function() {
          var $this = $(this),
            $bgSection = $($this.data('anchor-target')),
            bgContainer = $this.parents('.js-parallax-bg'),
            bgSectionHeight = $bgSection.innerHeight();

          $this.css({
            height: bgSectionHeight * 1.5 + 'px',
          });

          bgContainer.css({
            height: bgSectionHeight * 0.8 + 'px',
            top: -(bgSectionHeight + 50) + 'px',
          });
        });
      }

      function skrollrInit() {
        if (isMobile.any()) {
          skrollr.init().destroy();
        } else {
          $.when(parallaxHeight()).then(function() {
            skrollr.init({
              forceHeight: false,
              smoothScrolling: true,
              smoothScrollingDuration: 80,
              easing: {
                vibrate: function(p) {
                  return Math.sin(p * 10 * Math.PI);
                },
              },
            });
          });
        }
      }

      function skrollrRefresh() {
        if (isMobile.any()) {
          skrollr.init().destroy();
        } else {
          skrollr.init().refresh();
        }
      }

      function actTouchList() {
        if ($(window).width() < 1000) {
          $('.features-list__item').addClass('act');
        } else {
          $('.features-list__item').removeClass('act');
        }
      }

      function scrollAnimate() {
        var windowHeight = $(window).height();
        var scrollPos = $(window).scrollTop();
        var scrollPoint = scrollPos + windowHeight;
        var $animEl = $('.anim-container');
        var $animElAnchor = $('.anim-anchor');

        $animEl.each(function() {
          var $this = $(this),
            offset = $this.data('offset'),
            animElPos = $this.offset().top,
            timeout = $this.data('timeout');
          if (scrollPoint > animElPos + offset) {
            $(this).addClass('animate');
          }
        });
        $animElAnchor.each(function() {
          var $this = $(this),
            offset = $this.data('offset'),
            timeout = $this.data('timeout'),
            anchor = $($this.data('animate-anchor')),
            anchorPos = anchor.offset().top;
          if (scrollPoint > anchorPos + offset) {
            $(this).addClass('animate');
          }
        });
      }

      parallaxHeight();
      skrollrInit();
      scrollAnimate();
      setTimeout(function() {
        loadAnimation();
      }, 2);
      setTimeout(function() {
        numAnim();
        skrollrRefresh();
      }, 500);
      actTouchList();

      // window scroll events
      $(window).scroll(function() {
        scrollAnimate();
      });

      // window resize events
      $(window).resize(function() {
        parallaxHeight();
        actTouchList();
      });
      /* eslint-enable */

    }

    render() {
      return <WrappedComponent {...this.props} />;
    }
  };
}
/* eslint-enable */
