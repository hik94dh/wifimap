import React from 'react';
import withApollo  from '../index';
import renderer from 'react-test-renderer';

it('withApollo testing', () => {
    const tree = renderer.create(<withApollo  />).toJSON();
    expect(tree).toMatchSnapshot();
});
