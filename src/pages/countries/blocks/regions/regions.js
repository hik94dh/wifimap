export default [
  {
    img: 'img/location1.png',
    label: 'North America',
    name: 'north_america',
  },
  {
    img: 'img/location2.png',
    label: 'South America',
    name: 'south_america',
  },
  {
    img: 'img/location3.png',
    label: 'Europe',
    name: 'europe',
  },
  {
    img: 'img/location4.png',
    label: 'Asia Pacific',
    name: 'asia_pacific',
  },
  {
    img: 'img/location5.png',
    label: 'Middle East',
    name: 'middle_east',
  },
  {
    img: 'img/location6.png',
    label: 'Africa',
    name: 'africa',
  },
];
