import React from 'react';
import regions from './regions';
import RegionItem from './region-item';
import { func, string } from 'prop-types';

const Regions = ({ active, handleChange, getRegion }) => {
  return (
    <div className="wrap">
      <h1 className="main-screen__title ta-c">Select Your Location</h1>
      <div data-tabs="countries-list" className="tabs-nav">
        <div className="main-nav main-nav--locations">
          {regions.map(region => (
            <RegionItem
              active={active}
              getRegion={getRegion}
              handleChange={handleChange}
              key={region.label}
              {...region}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default Regions;

Regions.propTypes = {
    active: string,
    handleChange: func,
    getRegion: func
};