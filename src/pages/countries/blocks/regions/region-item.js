import React from 'react';
import cl from 'classnames';
import { func, string } from 'prop-types';

const RegionItem = ({ active, getRegion, handleChange, name, label, img }) => (
  <div className="main-nav__list-item">
    <div
      className={cl('main-nav__link-item', 'tabs-nav-item', {
        act: active === name,
      })}
      onClick={() => {
        handleChange(name);
        getRegion(name);
      }}
    >
      <div className="main-nav__image">
        <img src={img} alt="" />
      </div>
      <span className="main-nav__link">{label}</span>
    </div>
  </div>
);

export default RegionItem;

RegionItem.propTypes = {
    active: string,
    getRegion: func,
    handleChange: func,
    img: string,
    label: string,
    name: string,
};