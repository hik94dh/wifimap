import React from 'react';
import Regions  from '../index';
import RegionItem   from '../index';
import renderer from 'react-test-renderer';

it('Regions testing', () => {
    const tree = renderer.create(<Regions  />).toJSON();
    expect(tree).toMatchSnapshot();
});

it('RegionItem  testing', () => {
    const tree = renderer.create(<RegionItem   />).toJSON();
    expect(tree).toMatchSnapshot();
});