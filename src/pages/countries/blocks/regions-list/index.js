import React from 'react';
import { Link } from 'react-router-dom';
import { arrayOf, shape, string } from 'prop-types';

const RegionsList = ({ regionCountries }) => (
  <div id="tabs-countries-list" className="tabs-container">
    <div id="countries-list">
      <div className="wrap">
        <div className="titles-list">
          <div className="parts-row parts-5 parts-divide parts-lg-4 parts-md-2 parts-collapse-sm">
            {regionCountries.map(({ name, slug, code }) => (
              <div className="col-item" key={slug}>
                <div className="titles-list__item">
                  <Link className="titles-list__link" to={`/countries/${slug}`}>
                    <span className="titles-list__image">
                      <img
                        src={`img/flags/${code.toLowerCase()}.svg`}
                        height="17px"
                        alt=""
                      />
                    </span>
                    {name}
                  </Link>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default RegionsList;

RegionsList.propTypes = {
  regionCountries: arrayOf(
    shape({
      name: string,
      slug: string,
      code: string,
    }),
  ),
};
