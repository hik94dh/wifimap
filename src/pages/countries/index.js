import React, { Component } from 'react';
import Regions from './blocks/regions';
import RegionsList from './blocks/regions-list';
import fetchCountriesByRegion from '@queries/fetchCountriesByRegion';
import { withApollo } from 'react-apollo';
import { string } from "prop-types";

class Content extends Component {
  state = {
    active: 'north_america',
  };

  componentDidMount() {
    this.getRegion(this.state.active);
  }

  handleChange = tabName =>
    this.setState({
      active: tabName,
      regionCountries: null,
    });

  getRegion = region => {
    this.props.client
      .query({
        query: fetchCountriesByRegion,
        variables: { region },
        fetchPolicy: 'network-only',
      })
      .then(res =>
        this.setState({
          regionCountries: res.data.countries
            .slice()
            .sort((a, b) => (a.name < b.name ? -1 : 1)),
        }),
      );
  };

  render() {
    const { regionCountries } = this.state;
    return (
      <div className="page page--inner">
        <Regions
          active={this.state.active}
          handleChange={this.handleChange}
          getRegion={this.getRegion}
        />
        {regionCountries && <RegionsList regionCountries={regionCountries} />}
      </div>
    );
  }
}

export default withApollo(Content);

Content.propTypes = {
    getRegion: string,
    handleChange: string,
    active: string,
};
