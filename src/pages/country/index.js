import React from 'react';
import { compose, graphql } from 'react-apollo';

import Banner from './blocks/banner';
import Cities from './blocks/cities';
import Description from './blocks/description';
import fetchCountry from '@queries/fetchCountry';
import Proof from '@components/proof';
import Share from '@components/share';
import withJquery from './withJquery';

import { arrayOf, string, shape } from 'prop-types';

const enhance = compose(
  withJquery,
  graphql(fetchCountry, {
    options: props => {
      return { variables: { id: props.match.params.id.split('-')[0] } };
    },
  }),
);

const Content = ({ data: { country } }) => {
  if (!country) {
    return <div>Loading...</div>;
  }
  const {
    cities,
    description,
    hotspots_count,
    name,
    region,
    wikipedia_link,
  } = country;
  return (
    <div className="page">
      <Banner name={name} hotspots_count={hotspots_count} region={region} />
      <div className="content-block">
        {description && (
          <Description
            name={name}
            description={description}
            wikipedia_link={wikipedia_link}
          />
        )}

        <Cities name={name} cities={cities} />
        <Proof />
        <Share />
      </div>
    </div>
  );
};

export default enhance(Content);

Content.propTypes = {
  country: shape({
    cities: arrayOf(string),
    description: string,
    name: string,
    wikipedia_link: string,
  }),
};
