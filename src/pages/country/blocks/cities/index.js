import React from 'react';
import { Link } from 'react-router-dom';
import splitNumber from '@utils/splitNumbers';
import { arrayOf, shape, string } from 'prop-types';

const Cities = ({ name, cities }) => {
  const topCount = 5;
  const limit = 40;

  const topCities = cities.slice(0, topCount);
  const restCities = cities
    .slice(topCount, limit + topCount)
    .sort((a, b) => b.hotspots_count - a.hotspots_count);

  const renderTopCities = topCities.map(
    ({ name, id, hotspots_count, slug }) => (
      <div className="col-item" key={id}>
        <Link className="places-list__item" to={`/cities/${slug}`}>
          <span className="places-list__image" />
          <h3 className="places-list__title">{name}</h3>
          <span className="places-list__note">
            {splitNumber(hotspots_count, ' ')} Free Wifi
          </span>
        </Link>
      </div>
    ),
  );

  const renderRestCities = restCities.map(
    ({ name, id, hotspots_count, slug }) => (
      <div className="col-item" key={id}>
        <div className="links-list__item">
          <Link className="links-list__link" to={`/cities/${slug}`}>
            {name}
            <span className="links-list__note">
              {splitNumber(hotspots_count, ' ')}
            </span>
          </Link>
        </div>
      </div>
    ),
  );

  return (
    <div className="section">
      <div className="wrap">
        <h2>Cities with free WiFi in {name}</h2>
        <div className="places-list">
          <div className="parts-row parts-5 parts-divide parts-divide-bottom parts-lg-3 parts-collapse-md">
            {renderTopCities}
          </div>
        </div>
        <hr />
        <div className="links-list">
          <div className="parts-row parts-5 parts-divide parts-lg-4 parts-md-3 parts-collapse-sm">
            {renderRestCities}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Cities;

Cities.propTypes = {
  name: string,
  cities: arrayOf(shape({})),
};
