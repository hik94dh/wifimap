import React from 'react';
import splitNumber from '@utils/splitNumbers';
import { capitalizeFirstLetter } from '@utils/splitNumbers';
import { Link } from 'react-router-dom';
import { number, string } from 'prop-types';

const Banner = ({ hotspots_count, name, region }) => {
  const formattedRegion =
    region &&
    region.split('_').reduce((acc, curr) => {
      acc += capitalizeFirstLetter(curr) + ' ';
      return acc;
    }, '');
  return (
    <div>
      <div
        data-0-top="transform:translate3d(0px, 0%, 0px); top: 0;"
        data-top-bottom="transform:translate3d(0px, 0%,0px); top: 0;"
        data-anchor-target=".main-screen"
        className="bg-section__parallax-bg js-parallax-bg"
      >
        <div
          data-top-top="transform:translate3d(0px, -10%, 0px);"
          data-top-bottom="transform:translate3d(0px, -50%, 0px);"
          data-anchor-target=".main-screen"
          className="bg-section__parallax-bg-image js-parallax-image"
        />
      </div>
      <div className="main-screen main-screen--dark">
        <div className="main-screen__container main-screen__container--narrow">
          <div className="main-screen__inner">
            <ul className="crumbs">
              <li className="crumbs__item">
                <Link to="/" className="crumbs__link ic-r ic-arr-r-xs-wh">
                  Home
                </Link>
              </li>
              <li className="crumbs__item">
                <Link
                  to="/countries"
                  className="crumbs__link ic-r ic-arr-r-xs-wh"
                >
                  Country list
                </Link>
              </li>
              {formattedRegion && (
                <li className="crumbs__item">
                  <Link
                    to="/countries"
                    className="crumbs__link ic-r ic-arr-r-xs-wh"
                  >
                    {formattedRegion}
                  </Link>
                </li>
              )}

              <li className="crumbs__item">
                <span className="crumbs__current">{name}</span>
              </li>
            </ul>
            <div className="numbers-tooltip">
              <div className="tooltip-ribbon">
                <div className="tooltip-ribbon__inner">
                  <span className="ic-r ic-wifi">Live</span>
                </div>
              </div>
              <div className="numbers-tooltip__inner">
                <div className="numbers-tooltip__numbers">
                  {splitNumber(hotspots_count)}
                </div>
              </div>
            </div>
            <div className="wrap">
              <h1 className="main-screen__title">Mobile Free WiFi in {name}</h1>
              <div className="main-screen__text">
                <p>Get the WiFi map of the best WiFi hotspots in {name}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Banner;

Banner.propTypes = {
  name: string,
  hotspots_count: number,
};
