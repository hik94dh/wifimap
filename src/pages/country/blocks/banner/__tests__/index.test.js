import React from 'react';
import Banner  from '../index';
import renderer from 'react-test-renderer';
import { StaticRouter } from 'react-router';

it('Banner testing', () => {
    const tree = renderer.create(
        <StaticRouter>
            <Banner  />
        </StaticRouter>
    ).toJSON();
    expect(tree).toMatchSnapshot();
});
