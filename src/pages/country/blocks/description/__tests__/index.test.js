import React from 'react';
import Description  from '../index';
import renderer from 'react-test-renderer';

it('Description testing', () => {
    const tree = renderer.create(<Description  />).toJSON();
    expect(tree).toMatchSnapshot();
});