import React from 'react';
import { string } from "prop-types";
// import RegionsList from "@countries/blocks/regions-list";

const Description = ({ name, description, wikipedia_link }) => (
  <div>
    <section className="section section--gray">
      <div className="text-content">
        <h2 className="title title--sm">About {name}</h2>
        <p className="accent">{description}</p>
        {wikipedia_link && (
          <a href={wikipedia_link} target="_blank" rel="noopener noreferrer">
            Wikipedia
          </a>
        )}
      </div>
    </section>
  </div>
);

export default Description;

Description.propTypes = {
    name: string,
    description: string,
    wikipedia_link: string,
};
