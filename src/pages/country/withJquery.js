import React, { Component } from 'react';
import $ from 'jquery';
// import 'owl.carousel/dist/assets/owl.carousel.css';
// eslint-disable-next-line
import skrollr from 'skrollr';

/* eslint-disable */
export default function withJquery(WrappedComponent) {
  return class extends Component {
    componentDidMount() {
      // parallax height
      function parallaxHeight() {
        var bgItem = $('.js-parallax-image');
        bgItem.each(function() {
          var $this = $(this),
            $bgSection = $($this.data('anchor-target')),
            bgContainer = $this.parents('.js-parallax-bg'),
            bgSectionHeight = $bgSection.innerHeight();

          $this.css({
            height: bgSectionHeight * 1.2 + 'px',
          });

          bgContainer.css({
            height: bgSectionHeight + 'px',
            top: -(bgSectionHeight + 50) + 'px',
          });
        });
      }

      function skrollrInit() {
        if (isMobile.any()) {
          skrollr.init().destroy();
        } else {
          $.when(parallaxHeight()).then(function() {
            skrollr.init({
              forceHeight: false,
              smoothScrolling: true,
              smoothScrollingDuration: 80,
              easing: {
                vibrate: function(p) {
                  return Math.sin(p * 10 * Math.PI);
                },
              },
            });
          });
        }
      }
      parallaxHeight();
      $(window).resize(function() {
        parallaxHeight();
      });
    }

    render() {
      return <WrappedComponent {...this.props} />;
    }
  };
}
/* eslint-enable */
