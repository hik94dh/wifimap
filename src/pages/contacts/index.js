import React from 'react';
import withJquery from './withJquery'

const Content = () => (
  <div>
    <div className="page page--inner">
      <div className="wrap wrap--no-padding-sm">
        <div className="main-screen__title ta-c">Contact Us</div>
        <div className="feedback-form">
          <div className="feedback-form__container">
            <div className="parts-row parts-3 parts-collapse-md">
              <div className="col-item">
                <div className="parts-row part-2 parts-center parts-collapse-sm">
                  <div className="col-item">
                    <div className="contacts-list">
                      <div className="contacts-list__item">
                        <div className="contacts-list__title">Email</div>
                        <div className="contacts-list__text">
                          <a href="mailto:support@wifimap.io">
                            support@wifimap.io
                          </a>
                        </div>
                      </div>
                      <div className="contacts-list__item">
                        <div className="contacts-list__title">Address</div>
                        <div className="contacts-list__text">
                          25 Broadway, fl 9<br />New York, NY 10004
                        </div>
                      </div>
                      <div className="contacts-list__item">
                        <div className="parts-row parts-divide">
                          <div className="col-item">
                            <a
                              href="https://twitter.com/wifimapapp"
                              className="contacts-list__socials-item"
                              target="_blank"
                              rel="noopener noreferrer"
                            >
                              <span className="fa fa-twitter" />
                            </a>
                          </div>
                          <div className="col-item">
                            <a
                              href="https://www.facebook.com/wifimap.io/"
                              className="contacts-list__socials-item"
                              target="_blank"
                              rel="noopener noreferrer"
                            >
                              <span className="fa fa-facebook" />
                            </a>
                          </div>
                          <div className="col-item">
                            <a
                              href="https://plus.google.com/+Wifiofflinemap"
                              className="contacts-list__socials-item"
                              target="_blank"
                              rel="noopener noreferrer"
                            >
                              <span className="fa fa-google-plus" />
                            </a>
                          </div>
                          <div className="col-item">
                            <a
                              href="https://www.instagram.com/wifimap/"
                              className="contacts-list__socials-item"
                              target="_blank"
                              rel="noopener noreferrer"
                            >
                              <span className="fa fa-instagram" />
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-item part-3x2">
                <div className="feedback-form__inner">
                  <div className="feedback-form__item">
                    <div className="parts-row parts-6 parts-center parts-collapse-sm">
                      <div className="col-item part-6x4">
                        <form
                          id="feedbackForm"
                          action="https://api.wifimap.io/contacts"
                          method="post"
                          noValidate="novalidate"
                        >
                          <div className="inp-wrap">
                            <input type="text" name="name" placeholder="Name" />
                          </div>
                          <div className="inp-wrap">
                            <input
                              type="email"
                              name="email"
                              placeholder="Email"
                            />
                          </div>
                          <div className="inp-wrap">
                            <div className="select">
                              <select
                                name="topic"
                                data-placeholder="Message subject"
                                className="parts-select"
                              >
                                <option value="Technical Support">
                                  Technical Support
                                </option>
                                <option value="Partnership">Partnership</option>
                                <option value="Marketing/PR">
                                  Marketing/PR
                                </option>
                                <option value="Investors">Investors</option>
                              </select>
                              <div className="select-txt">
                                <span className="placeholder">
                                  Message subject
                                </span>
                              </div>
                              <div
                                className="dropdown"
                                style={{ height: 'auto' }}
                              >
                                <ul>
                                  <li rel="Technical Support" className="act">
                                    <span>Technical Support</span>
                                  </li>
                                  <li rel="Partnership">
                                    <span>Partnership</span>
                                  </li>
                                  <li rel="Marketing/PR">
                                    <span>Marketing/PR</span>
                                  </li>
                                  <li rel="Investors">
                                    <span>Investors</span>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          </div>
                          <div className="inp-wrap">
                            <textarea
                              data-autoresize
                              placeholder="Message"
                              name="message"
                              defaultValue={''}
                            />
                          </div>
                          <div className="inp-wrap">
                            <button
                              type="submit"
                              className="btn btn-fill btn-lg ic-r ic-arr-r-sm-wh"
                            >
                              Send message
                            </button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default withJquery(Content);