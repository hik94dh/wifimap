import React, { Component } from 'react';
import $ from 'jquery';
import 'jquery-validation';

/* eslint-disable */
export default function withJquery(WrappedComponent) {
  return class extends Component {
    componentDidMount() {
      // custom select
      //// select initialize
      function selectCustomize(el) {
        el.each(function() {
          var $this = $(this),
            numberOfOptions = $(this).children('option').length,
            placeholderText = $(this).data('placeholder'),
            $selectedOption = $this.find(':selected');
          if (
            $(this)
              .parent()
              .hasClass('select')
          ) {
            return;
          } else {
            $this.wrap('<div class="select"></div>');
            $this.after('<div class="select-txt"><span></span></div>');
            var $styledSelect = $this.next('.select-txt').find('span');
            if ($this.attr('data-placeholder')) {
              $styledSelect.text(placeholderText).addClass('placeholder');
            } else {
              if ($selectedOption.length > 0) {
                $styledSelect.text($selectedOption.text());
              } else {
                $styledSelect.text(
                  $this
                    .children('option')
                    .eq(0)
                    .text(),
                );
              }
              $styledSelect.removeClass('placeholder');
            }

            $this.parents('.select').append('<div class="dropdown"></div>');
            var $list = $('<ul />').appendTo(
              $this.parents('.select').find('.dropdown'),
            );
            for (var i = 0; i < numberOfOptions; i++) {
              $('<li>', {
                text: $this
                  .children('option')
                  .eq(i)
                  .text(),
                rel: $this
                  .children('option')
                  .eq(i)
                  .val(),
                'data-img': $this
                  .children('option')
                  .eq(i)
                  .data('img'),
              }).appendTo($list);
            }
            $this
              .parents('.select')
              .find('.dropdown li')
              .wrapInner('<span/>');

            if ($this.hasClass('opt-images')) {
              $this
                .parents('.select')
                .addClass('has-images')
                .find('.dropdown li')
                .each(function() {
                  var img =
                    '<img class="opt-img" src="' + $(this).data('img') + '">';
                  $(this).prepend(img);
                });
              imgFirst =
                '<img class="opt-img" src="' +
                $this
                  .children('option')
                  .eq(0)
                  .data('img') +
                '">';
              $styledSelect.prepend(imgFirst);
            }
          }
        });
      }
      selectCustomize($('.parts-select'));

      //// select dropdown resize
      function selectDropSize() {
        var $selectDropdown = $('.select .dropdown');
        $selectDropdown.each(function() {
          var $drop = $(this);
          var dropPosTop = $drop.offset().top;
          var windowScroll = $(window).scrollTop();
          var dropPos = dropPosTop - windowScroll;
          var wh = $(window).height();
          var listHeight = $drop.find('ul').height();
          var dropH = wh - dropPos;

          if (listHeight > dropH) {
            $drop.css({
              height: dropH - 14 + 'px',
            });
          } else {
            $drop.css({
              height: 'auto',
            });
          }
        });
      }

      $(window).on('load scroll resize', function() {
        selectDropSize();
      });

      //// select field click events
      var $selectField = '.select .select-txt';
      $('body').on('click', $selectField, function() {
        var $select = $(this).parents('.select');
        $('.select')
          .not($select)
          .removeClass('open');
        if ($select.hasClass('open')) {
          $select.removeClass('open');
        } else {
          $select.addClass('open');
        }
        selectDropSize();
        return false;
      });

      //// select dropdown events
      var $select = $('.select');
      var $selectOption = '.select .dropdown li';
      $select.each(function() {
        $(this)
          .find('.dropdown li:first')
          .addClass('act');
      });

      $('body').on('click', $selectOption, function() {
        var val = $(this).text();
        var content = $(this).html();
        var $parentWrap = $(this).parents('.select');
        var $select = $parentWrap.find('select');
        var $selectTxt = $(this)
          .parents('.select')
          .find('.select-txt')
          .find('span');

        var selectOptionVal = $select
          .find('option:contains("' + val + '")')
          .val();

        $select.val(selectOptionVal).change();
        $selectTxt.html(content);
        $parentWrap.removeClass('open');
        $(this)
          .parents('.dropdown')
          .find('li')
          .removeClass('act');
        $(this).addClass('act');
        return false;
      });

      $('select').on('change', function() {
        var $selectTxt = $(this)
            .parents('.select')
            .find('.select-txt')
            .find('span'),
          value = $(this).val();
        if (value == $(this).data('placeholder')) {
          $selectTxt.addClass('placeholder');
        } else {
          $selectTxt.removeClass('placeholder');
        }
      });

      //// text select events
      $('body').on('input', '.text-select input', function() {
        var $parent = $(this).parents('.text-select');
        if ($(this).val().length > 1) {
          $parent.addClass('open');
        } else {
          $parent.removeClass('open');
        }
      });

      $('body').on('click', '.text-select .dropdown li', function() {
        var $parent = $(this).parents('.text-select'),
          $input = $parent.find('input'),
          text = $(this).text();
        $input.val(text);
        $parent.removeClass('open');
      });

      $(document).on('click', function(event) {
        if (
          $(event.target).closest('.select .dropdown, .text-select .dropdown')
            .length
        )
          return;
        $('.select, .text-select').removeClass('open');
        event.stopPropagation();
      });

      $.formsValidate = function() {
        var feedbackValidateRules = {
          rules: {
            name: {
              required: true,
            },
            email: {
              required: true,
            },
            message: {
              required: true,
            },
          },
          messages: {
            name: {
              required: '<span class="err-mess">Field is required</span>',
            },
            email: {
              required: '<span class="err-mess">Field is required</span>',
              email: '<span class="err-mess">Wrong format</span>',
            },
            message: {
              required: '<span class="err-mess">Field is required</span>',
            },
          },
          submitHandler: function() {
            $.ajax({
              type: 'POST',
              url: 'https://api.wifimap.io/contacts',
              data: $('#feedbackForm').serialize(),
              success: function() {
                alert(
                  'Thank you for contacting us. We have received your enquiry and will respond to you ASAP.',
                );
                $('#feedbackForm')
                  .get(0)
                  .reset();
              },
            });
          },
        };

        var $feedbackForm = $('#feedbackForm');

        function formsValidateInit() {
          $feedbackForm.validate(feedbackValidateRules);
        }

        formsValidateInit();
      };
      $.formsValidate();
    }
    render() {
      return <WrappedComponent {...this.props} />;
    }
  };
}
/* eslint-enable */
