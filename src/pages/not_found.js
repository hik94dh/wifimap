import React from 'react';

const NotFound = () =>
  <div>
    <div className="page page--inner">
      <div className="wrap wrap--no-padding-sm not_found_error_message">
        <h1>404 page not found</h1>
        <p>We are sorry but the page you are looking for does not exist.</p>
      </div>
    </div>
  </div>

export default NotFound;
