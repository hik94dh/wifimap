import React, { Component } from 'react';
import fetchCity from '@queries/fetchCity';
import Proof from '@components/proof';
import Share from '@components/share';
import { getCityId } from '@src/utils/query-string';
import { arrayOf, number, string, shape } from 'prop-types';
import { compose, graphql } from 'react-apollo';
import { withRouter } from 'react-router';
import { pathOr } from 'ramda';
import withMapApollo from '@components/withMapApollo';

import Banner from './blocks/banner';
import Description from './blocks/description';
import InfoPanel from './blocks/info-panel';
import Nearby from './blocks/nearby';
import Parallax from './blocks/parallax';
import withJquery from './withJquery';

const enhance = compose(
  withJquery,
  withRouter,
  withMapApollo,
  graphql(fetchCity, {
    options: props => ({
      variables: {
        id: getCityId(pathOr([], ['match', 'params', 'id'], props)),
      },
    }),
    props: ({ data: { city = null } }) => ({
      city,
    }),
  }),
);

class Content extends Component {
  render() {
    const { city, location: { pathname } } = this.props;
    const formattedPathname =
      pathname.substr(-1) === '/' ? pathname.slice(0, -1) : pathname;

    if (!city || !city.nearby_cities || !city.country || !city.hotspots_count) {
      return <div />;
    }

    const {
      center_lat,
      center_lng,
      country,
      description,
      hotspots_count,
      name,
      nearby_cities,
      wikipedia_link,
    } = city;

    return (
      <div className="page">
        <Parallax />
        <Banner
          country={country}
          hotspots_count={hotspots_count}
          name={name}
          handleOpenMap={this.handleOpenMap}
          pathname={formattedPathname}
        />
        <div className="content-block">
          <div className="section">
            {description && (
              <Description
                name={name}
                description={description}
                wikipedia_link={wikipedia_link}
              />
            )}
            <InfoPanel
              center_lat={center_lat}
              center_lng={center_lng}
              handleOpenMap={this.handleOpenMap}
              name={name}
              pathname={formattedPathname}
            />
          </div>

          <Proof />
          <Nearby name={name} nearby_cities={nearby_cities} />
          <Share />
        </div>
      </div>
    );
  }
}

export default enhance(Content);

Content.propTypes = {
  city: shape({
    center_lat: number,
    center_lng: number,
    description: string,
    nearby_cities: arrayOf(
      shape({
        name: string,
        slug: string,
        hotspots_count: number,
      }),
    ),
    wikipedia_link: string,
    country: shape({
      name: string,
      slug: string,
    }),
    hotspots_count: number,
    name: string,
  }),
};
