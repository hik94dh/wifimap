import React, { Component } from 'react';
import $ from 'jquery';
// import 'owl.carousel/dist/assets/owl.carousel.css';
// eslint-disable-next-line
import skrollr from 'skrollr';

/* eslint-disable */
export default function withJquery(WrappedComponent) {
  return class extends Component {
    componentDidMount() {
      var isMobile = {
        Android: function() {
          return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
          return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
          return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
          return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
          return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
          return (
            isMobile.Android() ||
            isMobile.BlackBerry() ||
            isMobile.iOS() ||
            isMobile.Opera() ||
            isMobile.Windows()
          );
        },
      };

      // parallax height
      function parallaxHeight() {
        var bgItem = $('.js-parallax-image');
        bgItem.each(function() {
          var $this = $(this),
            $bgSection = $($this.data('anchor-target')),
            bgContainer = $this.parents('.js-parallax-bg'),
            bgSectionHeight = $bgSection.innerHeight();

          $this.css({
            height: bgSectionHeight * 1.2 + 'px',
          });

          bgContainer.css({
            height: bgSectionHeight + 'px',
            top: -(bgSectionHeight + 50) + 'px',
          });
        });
      }

      function skrollrInit() {
        if (isMobile.any()) {
          skrollr.init().destroy();
        } else {
          $.when(parallaxHeight()).then(function() {
            skrollr.init({
              forceHeight: false,
              smoothScrolling: true,
              smoothScrollingDuration: 80,
              easing: {
                vibrate: function(p) {
                  return Math.sin(p * 10 * Math.PI);
                },
              },
            });
          });
        }
      }

      function skrollrRefresh() {
        if (isMobile.any()) {
          skrollr.init().destroy();
        } else {
          skrollr.init().refresh();
        }
      }

      skrollrInit();
      setTimeout(function() {
        skrollrRefresh();
      }, 500);

      /* eslint-enable */
    }

    render() {
      return <WrappedComponent {...this.props} />;
    }
  };
}
/* eslint-enable */
