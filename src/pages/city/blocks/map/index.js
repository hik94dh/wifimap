import React, { Component } from 'react';
import MapStatic from '@components/map-static';

export default class extends Component {
  render() {
    const { center } = this.props;
    return (
      <div className="map-content__item">
        <MapStatic center={center} width="1200" height="600"/>
      </div>
    );
  }
}
