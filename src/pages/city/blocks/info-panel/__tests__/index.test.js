import React from 'react';
import renderer from 'react-test-renderer';
import { MemoryRouter } from 'react-router-dom';
import InfoPanel  from '../index';


describe('InfoPanel testing', () => {
    test('InfoPanel testing', () => {
        const component = renderer
            .create(
                <MemoryRouter>
                    <InfoPanel />
                </MemoryRouter>
            )
            .toJSON();

        expect(component).toMatchSnapshot();
    });
});
