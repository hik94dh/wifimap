import React from 'react';
import { number, string } from 'prop-types';
import Map from '../map';
import ButtonMap from '../button-map';

const InfoPanel = ({
  name,
  center_lat,
  center_lng,
  handleOpenMap,
  pathname,
}) => (
  <div className="wrap">
    <div className="info-panel">
      <div className="parts-row-table parts-2 parts-collapse-lg">
        <div className="col-item">
          <div
            data-bottom-top="transform:translate3d(0px,80px,0px);"
            data-top-bottom="transform:translate3d(0px,-80px,0px);"
            className="info-panel__image info-panel__image--left"
          >
            <div className="map-content">
              {center_lat &&
                center_lng && (
                  <Map center={{ lat: center_lat, lng: center_lng }} />
                )}
            </div>
          </div>
        </div>
        <div className="col-item">
          <div className="info-panel__desc info-panel__desc--right">
            <h2 className="info-panel__title">
              Browse the Free WiFi Map of {name}
            </h2>
            <div className="info-panel__text">
              <p>
                Are you visiting this city? Or maybe you want to get to know the
                place with no concerns about the price for data roaming? Well,
                here is an Open WiFi map of {name}, which helps you to
                continuously be able to access internet in every city!
              </p>
            </div>
            <ButtonMap href={`${pathname}/map`} color="yellow" />
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default InfoPanel;

InfoPanel.propTypes = {
  name: string,
  center_lat: number,
  center_lng: number,
};
