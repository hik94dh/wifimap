import React from 'react';
import { string } from 'prop-types';

const Description = ({ description, wikipedia_link }) => (
  <div>
    <div className="text-content">
      <h2 className="title --sm">About City</h2>
      <p className="accent">{description}</p>
      {wikipedia_link && (
        <a href={wikipedia_link} target="_blank">
          Wikipedia
        </a>
      )}
    </div>
  </div>
);

export default Description;

Description.propTypes = {
  description: string,
  wikipedia_link: string,
};
