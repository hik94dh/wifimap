import React from 'react';
import { Link } from 'react-router-dom';
import { number, string, shape } from 'prop-types';
import splitNumber from '@utils/splitNumbers';
import ButtonMap from '../button-map';

const Banner = ({ country, hotspots_count, handleOpenMap, name, pathname }) => (
  <div className="main-screen main-screen--dark">
    <div className="main-screen__container">
      <div className="main-screen__inner">
        <ul className="crumbs">
          <li className="crumbs__item">
            <Link className="crumbs__link ic-r ic-arr-r-xs-wh" to="/">
              Home
            </Link>
          </li>
          <li className="crumbs__item">
            <Link className="crumbs__link ic-r ic-arr-r-xs-wh" to="/countries">
              Country list
            </Link>
          </li>
          <li className="crumbs__item">
            <Link
              className="crumbs__link ic-r ic-arr-r-xs-wh"
              to={`/countries/${country.slug}`}
            >
              {country.name}
            </Link>
          </li>
          <li className="crumbs__item">
            <span className="crumbs__current">{name}</span>
          </li>
        </ul>
        <div className="numbers-tooltip">
          <div className="tooltip-ribbon">
            <div className="tooltip-ribbon__inner">
              <span className="ic-r ic-wifi">Live</span>
            </div>
          </div>
          <div className="numbers-tooltip__inner">
            <div className="numbers-tooltip__numbers">
              {splitNumber(hotspots_count)}
            </div>
          </div>
        </div>
        <div className="wrap">
          <div className="main-screen__title">
            <h1>Mobile Free WiFi in {name}</h1>
          </div>
          <div className="main-screen__text">
            <p>Get the WiFi map of the best WiFi hotspots in {name}</p>
          </div>
          <ButtonMap href={`${pathname}/map`} />
        </div>
      </div>
    </div>
  </div>
);

export default Banner;

Banner.propTypes = {
  country: shape({
    name: string,
    slug: string,
  }),
  hotspots_count: number,
  name: string,
};
