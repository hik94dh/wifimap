import React from 'react';
import Parallax  from '../index';
import renderer from 'react-test-renderer';

it('Parallax testing', () => {
    const tree = renderer.create(<Parallax   />).toJSON();
    expect(tree).toMatchSnapshot();
});
