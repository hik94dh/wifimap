import React from 'react';

export default () => (
  <div
    data-0-top="transform:translate3d(0px, 0%, 0px); top: 0;"
    data-top-bottom="transform:translate3d(0px, 0%,0px); top: 0;"
    data-anchor-target=".main-screen"
    className="bg-section__parallax-bg bg-section__parallax-bg--fixed js-parallax-bg"
  >
    <div
      style={{ backgroundImage: 'url(\'/img/city-bg@2x.jpg\')'}}
      data-top-top="transform:translate3d(0px, 0, 0px);"
      data-top-bottom="transform:translate3d(0px, -50%, 0px);"
      data-anchor-target=".main-screen"
      className="bg-section__parallax-bg-image js-parallax-image"
    />
  </div>
);
