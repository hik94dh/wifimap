import React from 'react';
import cl from 'classnames';
import { Link } from 'react-router-dom';
import { oneOf, string } from 'prop-types';

const ButtonMap = ({ color, href, pathname }) => (
  <Link className="btn-icon btn" to={href}>
    <span
      className={cl('btn-icon__icon', {
        'btn-icon__icon--blue': color === 'blue',
        'btn-icon__icon--yellow': color === 'yellow',
      })}
    >
      <span className="ic-bg ic-place" />
    </span>
    <span className="btn-icon__text">
      <span className="ic-r ic-arr-r-sm">Open map</span>
    </span>
  </Link>
);

ButtonMap.defaultProps = {
  color: 'blue',
};

ButtonMap.propTypes = {
  color: oneOf(['blue', 'yellow']),
  href: string,
  pathname: string,
};

export default ButtonMap;
