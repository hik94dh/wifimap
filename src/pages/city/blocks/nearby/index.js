import React from 'react';
import { arrayOf, number, string, shape } from 'prop-types';

const Nearby = ({ name, nearby_cities }) => {
  return (
    <div className="section">
      <div className="wrap">
        <div className="section-title">
          <h3 className="section-title__title">
            Cities with free WiFi near {name}
          </h3>
        </div>
        <div className="titles-list">
          <div className="parts-row parts-5 parts-divide parts-divide-bottom parts-lg-3 parts-md-2 parts-collapse-sm">
            {nearby_cities.map(({ name, hotspots_count, slug }) => (
              <div className="col-item" key={slug}>
                <a href={`/cities/${slug}`} className="titles-list__item">
                  <h4 className="titles-list__title">{name}</h4>
                  <span className="titles-list__note">
                    {hotspots_count} Free Wifi
                  </span>
                </a>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Nearby;

Nearby.propTypes = {
  name: string,
  nearby_cities: arrayOf(
    shape({
      name: string,
      slug: string,
      hotspots_count: number,
    }),
  ),
};
