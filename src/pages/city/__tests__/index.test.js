import React from 'react';
import enhance  from '../index';
import renderer from 'react-test-renderer';

it('enhance testing', () => {
    const tree = renderer.create(<enhance  />).toJSON();
    expect(tree).toMatchSnapshot();
});
