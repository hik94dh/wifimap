import React from 'react';
import NotFound  from '../not_found';
import Privacy from '../privacy';
import Terms  from '../terms';
import renderer from 'react-test-renderer';

it('NotFound  testing', () => {
    const tree = renderer.create(<NotFound  />).toJSON();
    expect(tree).toMatchSnapshot();
});

it('Privacy  testing', () => {
    const tree = renderer.create(<Privacy />).toJSON();
    expect(tree).toMatchSnapshot();
});

it('Terms  testing', () => {
    const tree = renderer.create(<Terms   />).toJSON();
    expect(tree).toMatchSnapshot();
});