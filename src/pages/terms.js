import React from 'react';

const Terms = () =>
  <div>
    <p>
      WiFi Map – Terms of Use<br/>
      August 4, 2017
    </p>
    <p>
      These terms of use (“Terms”) apply to persons anywhere in the world (each such person, a “User” or “you”) who install, access or use the mobile application (“App”) published by WiFi Map LLC (“WiFi Map” or “we”).  PLEASE READ THESE TERMS CAREFULLY, AS THEY ARE A LEGAL AGREEMENT BETWEEN YOU AND WIFI MAP. 
    </p>
    <p>
      PLEASE BE ADVISED: THESE TERMS CONTAIN PROVISIONS THAT GOVERN HOW CLAIMS THAT YOU AND WIFI MAP HAVE AGAINST EACH OTHER CAN BE BROUGHT (SEE SECTION 4 BELOW).  THOSE PROVISIONS WILL, WITH LIMITED EXCEPTIONS, REQUIRE YOU TO SUBMIT CLAIMS YOU HAVE AGAINST WIFI MAP TO BINDING AND FINAL ARBITRATION ON AN INDIVIDUAL BASIS, NOT AS A PLAINTIFF OR CLASS MEMBER IN ANY CLASS, GROUP OR REPRESENTATIVE ACTION OR PROCEEDING.
    </p>
    <p>
      In order to install, access or use the App, you must be of legal age for those activities in the jurisdiction in which you are located.  If you are not of legal age, you must not install, access or use the App.  
    </p>
    <p>
      1. <u>Binding Agreement; Termination</u>
    </p>
    <p>
      Your installation of, access to or use of our App constitutes your binding agreement to those Terms, including the dispute resolution and arbitration provisions.  If at any time you disagree with these Terms, you must refrain from installing our App, or, if you have already installed our App, refrain from accessing or using it and delete it.  These Terms contain the entire agreement between you and WiFi Map, and supersede any prior agreements or arrangements with you that relate in any way to WiFi Map or the App.  
    </p>
    <p>
      WiFi Map reserves the right to immediately terminate these Terms and any services provided by WiFi Map, including its publication of the App and provision of any content, information, functionality or other services through the App, at any time, either with respect to you or generally, for any reason, including your violation of these terms, or for no reason.  The preamble above, this <u>Section 1, 2–4  and Sections 6–19</u> shall survive the termination of these Terms.  
    </p>
    <p>
      2. <u>Governing Law</u>
    </p>
    <p>
      These Terms, and any claims, disputes or controversies related to your use of the App or any content, information and related materials (“<u>Content</u>”) provided through the App or otherwise related to the relationship between you and WiFi Map, whether contractual or non-contractual, are governed by the laws of the State of New York, United States of America, without giving effect to any choice or conflict of law provision or rule, whether in the State of New York or any other jurisdiction, that would cause the laws of any jurisdiction other than the State of New York to apply.
    </p>
    <p>
      3. <u>Jurisdiction and Venue for Claims not Subject to Arbitration</u>
    </p>
    <p>
      The New York state and United States federal courts sitting in New York County, New York shall have exclusive jurisdiction over any claims, disputes or controversies related to these Terms, your use of the App or any Content provided through the App, or otherwise related to the relationship between you and WiFi Map, whether contractual or non-contractual, that are not subject to mandatory arbitration under <u>Section 4</u>. 
    </p>
    <p>
      4. <u>Mandatory Binding Arbitration</u>
    </p>
    <p>
      <u><i>(a) Agreement to Arbitrate</i></u>
    </p>
    <p>
      Any claims, disputes or controversies related to these Terms, your use of the App or any Content provided through the App, or otherwise related to the relationship between you and WiFi Map, whether based in contract, tort, statute, fraud, misrepresentation, or any other legal theory, regardless of when they arise and regardless of any termination of these Terms, shall be determined by mandatory binding individual (not class) arbitration.  
    </p>
    <p>
      The arbitrator shall have the exclusive power to rule on his or her own jurisdiction, including any objections with respect to the existence, scope or validity of the this agreement to arbitrate or to the arbitrability of any claim or counterclaim.  Arbitration is more informal than a lawsuit in court.  THERE IS NO JUDGE OR JURY IN ARBITRATION, AND COURT REVIEW OF AN ARBITRATION AWARD IS LIMITED.  There may be more limits on the availability and scope of discovery than in court. The arbitrator must follow these Terms and can award the same damages and relief as a court (including attorney fees), except that the arbitrator may not award any relief, including declaratory or injunctive relief, benefiting anyone but the parties to the arbitration.  This arbitration provision will survive termination of these Terms.
    </p>
    <p>
      <u><i>(b) Exceptions</i></u>
    </p>
    <p>
      Notwithstanding <u>Section 4(a)</u>, nothing in these Terms, including this <u>Section 4</u>, will be deemed to waive, preclude, or otherwise limit either of our rights, at any time, to (i) bring an individual action in a U.S. small claims court for a matter within the jurisdiction of that court or (ii) bring an individual action seeking only temporary or preliminary individualized injunctive relief in a court of law, pending a final ruling from the arbitrator.  In addition, the agreement to arbitrate in this <u>Section 4</u> doesn’t stop you or us from bringing issues to the attention of federal, state, or local agencies.  Those agencies can, if the law allows, seek relief against us on your behalf (or vice versa).
    </p>
    <p>
      <u><i>(c) No Class or Representative Proceedings: Class Action Waiver</i></u>
    </p>
    <p>
      YOU AND WIFI MAP EACH MAY BRING CLAIMS AGAINST THE OTHER ONLY IN YOUR OR ITS INDIVIDUAL CAPACITY AND NOT AS A PLAINTIFF OR CLASS MEMBER IN ANY PURPORTED CLASS OR REPRESENTATIVE ACTION.  Unless both you and WiFi Map agree, no arbitrator or judge may consolidate more than one person’s claims or otherwise preside over any form of a representative or class proceeding.  The arbitrator may award injunctive relief only in favor of the individual party seeking relief and only to the extent necessary to provide relief warranted by that party’s individual claim. If a court decides that applicable law precludes enforcement of any of this paragraph’s limitations as to a particular claim for relief, then that claim (and only that claim) must be severed from the arbitration and may be brought in court.
    </p>
    <p>
      <u><i>(d) Arbitration Rules</i></u>
    </p>
    <p>
      Either you or WiFi Map may start arbitration proceedings.  Any arbitration between you and WiFi Map will take place in the English language under the Consumer Arbitration Rules of the American Arbitration Association (“<u>AAA</u>”) then in force (the “<u>AAA Rules</u>”), as modified by this <u>Section 4</u>.  The Federal Arbitration Act shall apply to, and govern the interpretation and enforcement of, this provision (despite the choice of law provision above).  The AAA Rules, as well as instructions on how to file an arbitration proceeding with the AAA, appear at adr.org, or you may call the AAA at 1-800-778-7879.  WiFi Map can also help put you in touch with the AAA.
    </p>
    <p>
      Any arbitration hearings will take place in the county (or parish) of your residence, provided that if the claim is for $25,000 or less, you may choose whether the arbitration will be conducted (i) solely on the basis of documents submitted to the arbitrator; (ii) through a non-appearance based telephonic hearing; or (iii) by an in-person hearing as established by the AAA Rules in the county (or parish) of your residence.
    </p>
    <p>
      If you choose to file an arbitration proceeding and you are required to pay a filing fee, WiFi Map will reimburse you for that filing fee, unless your claim is for greater than US $10,000, in which case you will be responsible for the filing fee.  WiFi Map will pay any other arbitration fees, including your share of arbitrator compensation, unless otherwise required by the AAA Rules or court order.  Regardless of the manner in which the arbitration is conducted, the arbitrator shall issue a reasoned written decision sufficient to explain the essential findings and conclusions on which the decision and award, if any, are based.  The arbitrator may make rulings and resolve disputes as to the payment and reimbursement of fees or expenses at any time during the proceeding and upon request from either party made within 14 days of the arbitrator’s ruling on the merits.
    </p>
    <p>
      <u><i>(e) Notice; Process</i></u>
    </p>
    <p>
      A party who intends to seek arbitration must first send a written notice of the dispute to the other, by certified mail, Federal Express, UPS, or Express Mail (signature required), or if we do not have a physical address on file for you, by electronic mail (“<u>Notice</u>”).  WiFi Map’s address for Notice is: WiFi Map LLC, Attn: President, 25 Broadway, 9th Floor, New York, NY 10004, USA.  The Notice must (i) describe the nature and basis of the claim or dispute; and (ii) set out the specific relief sought (“<u>Demand</u>”).  You and WiFi Map each shall use good faith efforts to resolve the claim directly, but if we do not reach an agreement to do so within 30 days after the Notice is received, you or WiFi Map may commence an arbitration proceeding.  During the arbitration, the amount of any settlement offer made by you or WiFi Map shall not be disclosed to the arbitrator until after the arbitrator makes a final decision and award, if any.  All documents and information disclosed in the course of the arbitration shall be kept strictly confidential by the recipient and shall not be used by the recipient for any purpose other than for purposes of the arbitration or the enforcement of the arbitrator’s decision and award and shall not be disclosed except in confidence to persons who have a need to know for such purposes or as required by applicable law.
    </p>
    <p>
      <u><i>(f) Enforceability</i></u>
    </p>
    <p>
      If the agreement to arbitrate in this <u>Section 4</u> is invalidated in whole or in part, the exclusive jurisdiction and venue described in <u>Section 3</u> shall govern any claim in court arising out of or related to these Terms.
    </p>
    <p>
      5. <u>Grant of License</u>
    </p>
    <p>
      Subject to your compliance with these Terms, WiFi Map hereby grants you a limited, non-exclusive, non-sublicensable, revocable, non-transferable license to install, access and use the App on your personal mobile device solely for the purpose of accessing and using any Content provided through the App to locate and access WiFi hotspots, solely for your own personal use, and not for (a) you to share with any other individual or entity or (b) any commercial use whatsoever.  You shall not use any Content provided through the App to build, expand or validate any database of WiFi hotspot information, nor shall you use the Content provided through the App to provide any WiFi hotspot information to any other individual or entity.
    </p>
    <p>
      6. <u>Ownership</u>
    </p>
    <p>
      The App, and all Content that WiFi Map provides through the App, are and shall remain WiFi Map’s property or the property of WiFi Map’s licensors.  Neither these Terms nor your use of the App convey or grant to you any rights (a) in or related to the App or any Content, except for the limited license granted in <u>Section 5</u>, or (b) to use or reference in any manner WiFi Map’s company names, logos, product and service names, trademarks or service marks, or those of WiFi Map’s licensors.
    </p>
    <p>
      7. <u>Restrictions</u>
    </p>
    <p>
      You shall not (a) remove any copyright, trademark or other proprietary notices from the App or any Content provided through the App; (b) reproduce, modify, prepare derivative works based upon, distribute, license, lease, sell, resell, transfer, publicly display, publicly perform, transmit, stream, broadcast or otherwise exploit the App or any Content provided through the App, except as expressly permitted by WiFi Map in writing; (c) decompile, reverse engineer or disassemble the App or any Content provided through the App; (d) link to, mirror or frame the App or any Content provided through the App; (e) cause or launch any programs or scripts for the purpose of scraping, indexing, surveying, or otherwise data mining the App or any Content provided through the App or unduly burdening or hindering the operation or functionality of any aspect of the services that we provide through the App; or (f) attempt to gain unauthorized access to or impair any aspect of the App or its related systems or networks.
    </p>
    <p>
      Without limiting the generality of the immediately preceding paragraph, you shall use any information that you obtain through the App concerning WiFi hotspots <u>solely</u> for the purpose of accessing those hotspots yourself, and not for <u>any</u> other purpose.  
    </p>
    <p>
      8. <u>Privacy Policy</u>
    </p>
    <p>
      Your installation of, access to and use of our App constitutes your agreement to our privacy policy in effect from time to time, which is available here: <a href="http://www.wifimap.io/privacy">http://www.wifimap.io/privacy</a> (the “<u>Policy</u>”).  If at any time you disagree with the Policy, you must refrain from installing our App, or, if you have already installed our App, refrain from accessing or using it and delete it.
    </p>
    <p>
      9. <u>Third Party Services and Content</u>
    </p>
    <p>
      Our App, and the Content that we provide through our App, may incorporate, or be made available or accessed in connection with, third party services and content, including advertising, the we do not control.  Different terms of use and privacy policies may apply to your use of those third party services and content.  We do not endorse those third party services and content, and in no event shall we be responsible or liable for any acts, omissions, products or services of those third party providers.  
    </p>
    <p>
      10. <u>User Content</u>
    </p>
    <p>
      <u><i>(a) Uploads of User Content</i></u>
    </p>
    <p>
      The primary function of our App is crowdsourcing from our Users information about WiFi hotspots and making that information available to our Users to help them locate and access WiFi hotspots.  Accordingly, we may, in our sole discretion, permit you from time to time to submit, upload or otherwise make available to us, through the App, information about WiFi hotspots and other related information (“<u>User Content</u>”).  We are not obliged to accept, retain or make available any User Content.
    </p>
    <p>
      You shall not submit, upload or otherwise provide to us any User Content that is false, erroneous, misleading, illegal or malicious, nor shall you submit, upload or otherwise provide to us any User Content if your doing so violates the rights of any hotspot owner or operator or other third party.  You shall be solely responsible for any obligation to pay any royalties or other amounts in connection with any use or exploitation of any User Content.
    </p>
    <p>
      <u><i>(b) Grant of License to User Content</i></u>
    </p>
    <p>
      By providing User Content to WiFi Map, you grant WiFi Map a worldwide, perpetual, irrevocable, transferable, royalty-free license, with the right to sublicense, to use, copy, modify, create derivatives works of, distribute, publicly display, publicly perform and otherwise exploit in any manner, commercial or otherwise, that User Content in all formats and distribution channels now known or hereafter devised, including disseminating that User Content to other Users through the App, without further notice to or consent from you, and without the requirement of payment to you or any other individual or entity.
    </p>
    <p>
      <u><i>(c) Sufficient Rights in User Content</i></u>
    </p>
    <p>
      You represent and warrant that (a) you either are the sole and exclusive owner of all User Content or you have all rights, licenses, consents and releases necessary to grant WiFi Map the license to the User Content as set out above, free and clear of any obligations on WiFi Map’s part, including any obligation to pay any royalties; and (b) neither the User Content, nor your submission, uploading or otherwise making available the User Content, nor our use of the User Content as permitted by these Terms, will infringe, misappropriate or violate a third party’s intellectual property or proprietary rights, or rights of publicity or privacy, or result in the violation of any applicable law or regulation.
    </p>
    <p>
      Without limiting the generality of the immediately preceding paragraph, you represent and warrant that, in each instance in which you upload the password to a WiFi hotspot through our App, or otherwise make a password for a hotspot available to us, you are the owner of the hotspot, or have been authorized by the owner of the hotspot to upload the password through our App or otherwise make it available to us.  IF YOU ARE NOT THE OWNER OF A HOTSPOT, AND YOU HAVE NOT BEEN AUTHORIZED BY THE OWNER TO UPLOAD THE PASSWORD THROUGH OUR APP OR OTHERWISE MAKE IT AVAILABLE TO US, YOU MUST NOT UPLOAD THE PASSWORD THROUGH OUR APP OR OTHERWISE MAKE IT AVAILABLE TO US.
    </p>
    <p>
      11. <u>Network Access and Devices</u>
    </p>
    <p>
      You are responsible for obtaining the mobile network or WiFi access necessary to use our App.  Your mobile network’s data rates and fees may apply if you install, access or use the App from your mobile device.  You are responsible for acquiring and updating compatible devices and operating systems necessary to access and use the App and any updates to the App.  We do not guarantee that the App, or any function of the App, will function on any particular device or with any particular operating system.  In addition, the use of the App may be subject to malfunctions and delays inherent in the use of the Internet and electronic communications.  
    </p>
    <p>
      12. <u>Disclaimers</u>
    </p>
    <p>
      THE APP, AND THE CONTENT MADE AVAILABLE THROUGH IT, ARE PROVIDED “AS IS” AND “AS AVAILABLE.”  WIFI MAP DISCLAIMS ALL REPRESENTATIONS AND WARRANTIES, EXPRESS, IMPLIED OR STATUTORY, NOT EXPRESSLY SET OUT IN THESE TERMS, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.  

    </p>
    <p>
      IN ADDITION, WIFI MAP MAKES NO REPRESENTATION, WARRANTY OR GUARANTEE CONCERNING, AND SHALL NOT BE LIABLE OR OTHERWISE RESPONSIBLE FOR, (A) THE RELIABILITY, INCLUDING SERVICE AVAILABILITY, OF THE APP, (B) THE ACCURACY AND CURRENCY OF THE CONTENT MADE AVAILABLE THROUGH THE APP, (C) THE INTERCEPTION OF ANY DATA THAT WE PROVIDE TO YOU OR YOU PROVIDE TO US THROUGH THE APP, (D) ANY MATTERS RESULTING FROM OR OTHERWISE RELATING TO ANY UNAUTHORIZED ACCESS BY ANY THIRD PARTY TO YOUR ACCOUNT OR (E) THE EXISTENCE, AVAILABILITY, PERFORMANCE, SAFETY OR SECURITY OF ANY WIFI HOTSPOT.  WIFI HOTSPOTS ABOUT WHICH INFORMATION IS PROVIDED THROUGH THE APP ARE OPERATED BY THIRD PARTIES AND ARE NOT UNDER WIFI MAP’S CONTROL.  ACCORDINGLY, WIFI MAP SHALL HAVE NO LIABILITY OR OTHER RESPONSIBILITY CONCERNING ANY HOTSPOT.

    </p>
    <p>
      WITHOUT LIMITING THE GENERALITY OF THE PRECEDING PROVISIONS OF THIS SECTION 12, THE WIFI HOTSPOT INFORMATION MADE AVAILABLE THROUGH THE APP IS CROWDSOURCED AND MAY BE INCOMPLETE, OUT OF DATE OR OTHERWISE ERRONEOUS, AND WIFI MAP SHALL NOT BE LIABLE OR OTHERWISE RESPONSIBLE THEREFOR.  THE ENTIRE RISK ARISING OUT OF YOUR USE OF THE APP AND ANY CONTENT, INCLUDING WIFI HOTSPOTS AND PASSWORDS, PROVIDED THROUGH THE APP REMAINS SOLELY WITH YOU, TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW.

    </p>
    <p>
      ADVERTISEMENTS, INFORMATION AND ALL OTHER CONTENT PUBLISHED BY THIRD PARTIES ARE THE SOLE RESPONSIBILITY OF THE THIRD PARTY PUBLISHERS, AND WIFI MAP HAS NO LIABILITY OR OTHER RESPONSIBILITY FOR THOSE MATERIALS, EVEN IF THEY ARE PRESENTED THROUGH OUR APP OR THROUGH THE USE OF DATA COLLECTED THROUGH OUR APP.  IF YOU CHOOSE TO USE ANY OFFERS OF ANY ADVERTISER OR OTHER THIRD-PARTY PUBLISHER YOU DO SO AT YOUR OWN RISK, AND YOU MUST ALWAYS VERIFY THE CREDIBILITY OF THE PUBLISHER.  

    </p>
    <p>
      13. <u>Limitation of Liability</u>
    </p>
    <p>
      WIFI MAP SHALL NOT BE LIABLE FOR INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, LOST DATA, PERSONAL INJURY, OR PROPERTY DAMAGE RELATED TO, IN CONNECTION WITH, OR OTHERWISE RESULTING FROM ANY USE OF THE APP OR ANY CONTENT MADE AVAILABLE THROUGH THE APP, REGARDLESS OF THE NEGLIGENCE (EITHER ACTIVE, AFFIRMATIVE, SOLE, OR CONCURRENT) OF WIFI MAP, EVEN IF WIFI MAP HAS BEEN ADVISED OF THE POSSIBILITY OF THOSE DAMAGES.

    </p>
    <p>
      WITHOUT LIMITING THE GENERALITY OF THE IMMEDIATELY PRECEDING PARAGRAPH, WIFI MAP SHALL NOT BE LIABLE OR OTHERWISE RESPONSIBLE FOR YOU USE OF OR RELIANCE ON THE APP OR ANY CONTENT MADE AVAILABLE THROUGH THE APP, NOR FOR YOUR INABILITY TO ACCESS OR USE THE APP OR ANY CONTENT MADE AVAILABLE THROUGH THE APP.  

    </p>
    <p>
      14. <u>Indemnity</u>
    </p>
    <p>
      You shall indemnify, defend and hold harmless WiFi Map and its members, shareholders, managers, directors, officers, employees and agents from and against any and all claims, demands, losses, liabilities and expenses, including reasonable fees of attorneys, accountants and other professionals, arising out of or in connection with (a) your use of the App or any Content made available through the App, (b) your breach or violation of any of these Terms, (c) WiFi Map’s, and its licensees’, use of your User Content or (iv) your violation of the rights of any third party, including the owner or operator of any WiFi hotspot.  

    </p>
    <p>
      15. <u>Notice</u>
    </p>
    <p>
      We may give notice to you by means of a general notice through the App or on our website, electronic mail to any address associated with your user account, telephone or text message to any phone number associated with your user account or by written communication sent by certified mail, Federal Express, UPS or Express Mail to any address associated with your user account.  Notices that we deliver by a general notice through the App or on our website shall be deemed delivered when posted, notices that we deliver by electronic mail, telephone or text message shall be deemed delivered when dispatched, notices that we deliver by Federal Express, UPS or Express Mail shall be deemed delivered 1 business day after we dispatch them and notices that we deliver by certified mail shall be deemed delivered 3 business days after we dispatch them.  You may give notice to WiFi Map by certified mail, Federal Express, UPS or Express Mail (in each case signature required) to WiFi Map LLC, Attn: Legal Department, 25 Broadway, 9th Floor, New York, NY 10004, USA, which shall be deemed effective and received when actually received by WiFi Map.  Notwithstanding the preceding provisions of this <u>Section 15</u>, you and we each shall give all Notices of Demands as provided in <u>Section 4(e)</u>.

    </p>
    <p>
      16. <u>Assignment</u>
    </p>
    <p>
      You shall not assign these Terms without WiFi Map’s prior written approval, and any purported assignment without that approval shall be void and of no effect.  WiFi Map may assign these Terms without your consent to any subsidiary or affiliate and to any purchaser of all or substantially all of its business to which these Terms relate.  
    </p>
    <p>
      17. <u>Severability</u>
    </p>
    <p>
      If any provision of these Terms would be held in any jurisdiction to be invalid, prohibited or unenforceable for any reason, that provision, as to that jurisdiction, shall be ineffective, without invalidating the remaining provisions of these Terms or affecting the validity or enforceability of that provision in any other jurisdiction.  Notwithstanding the foregoing, if that provision could be more narrowly drawn so as not to be invalid, prohibited or unenforceable in that jurisdiction, it shall, as to that jurisdiction, be so narrowly drawn, without invalidating the remaining provisions of these Terms or affecting the validity or enforceability of that provision in any other jurisdiction.
    </p>
    <p>
      18. <u>Interpretation</u>
    </p>
    <p>
      The use in these Terms of the term “<u>includes</u>” or “<u>including</u>” means “<u>includes or including, without limitation</u>.”  Where specific language is used to clarify by example a general statement contained in these Terms, that specific language shall not be deemed to modify, limit or restrict in any manner the construction of the general statement to which it relates.  All references to Sections mean Sections of these Terms.  The Section and subsection headings in these Terms are for convenience of reference only and shall not govern or affect the interpretation of any of the provisions of these Terms.  The word “or” is not exclusive.  The use in these Terms of the masculine, feminine or neuter forms shall also denote the other forms, as in each case the context requires.  

    </p>
    <p>
      19. <u>Amendments</u>
    </p>
    <p>
      We may amend these Terms from time to time by posting an updated version of these Terms on our website or through our App, which will take effect when so posted.  Your continued use of our App after any such update will constitute your agreement to the updated Terms.  Accordingly, we encourage you to periodically review these Terms.
    </p>
  </div>

export default Terms;
