import React, { Component } from 'react';
import api from '@src/api';
import Loader from '@components/loader';

import GoogleMap from './GoogleMap';
import './styles.scss';


export default class SimpleMap extends Component {
  get mapUrl() {
    return `https://maps.googleapis.com/maps/api/js?key=${
      api.map
    }&v=3.exp&libraries=geometry,drawing,places`;
  }
  render() {
    const { isLoadingCoords } = this.props;
    return (
      <div className="map__content">
        {isLoadingCoords ? (
          <div className="overlay">
            <Loader />
          </div>
        ) : (
          <GoogleMap
            googleMapURL={this.mapUrl}
            loadingElement={<div className="map__loading" />}
            containerElement={<div className="map__container" />}
            mapElement={<div id="google-map" className="map__element" />}
            {...this.props}
          />
        )}
      </div>
    );
  }
}
