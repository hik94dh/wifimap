import { compose } from 'react-apollo';
import {
  GoogleMap,
  InfoWindow,
  Marker,
  withGoogleMap,
  withScriptjs,
} from 'react-google-maps';
import { Component } from 'react';
import MarkerInfo from '../marker-info';
import MarkerClusterer from 'react-google-maps/lib/components/addons/MarkerClusterer';
import React from 'react';

const enhance = compose(
  withScriptjs,
  withGoogleMap,
);

class MapComponent extends Component {
  handleMarkerClick = uuid => () => {
    return this.props.handleMarkerClick(uuid);
  };

  renderMarker = hotspot => {
    if (!hotspot) {
      return null;
    }

    const { activeHotspot, activeId, deactivateHotspot } = this.props;
    const { uuid, lat, lng } = hotspot;
    const isActive = activeId === uuid;
    const markerIcon = {
      url: '/img/map-marker-st.png',
      scaledSize: {
        height: 52,
        width: 56,
      },
      // eslint-disable-next-line
      anchor: new google.maps.Point(17, 48),
    };
    const activeMarkerIcon = {
      ...markerIcon,
      url: '/img/map-marker-active.png',
    };

    if (isActive) {
      return (
        <Marker
          key={uuid}
          position={{ lat, lng }}
          icon={activeMarkerIcon}
          onClick={this.handleMarkerClick(uuid)}
        >
          <InfoWindow
            options={{
              // eslint-disable-next-line
              pixelOffset: new google.maps.Size(-10, 0),
              maxWidth: 280,
            }}
            onCloseClick={deactivateHotspot}
          >
            <MarkerInfo {...activeHotspot} />
          </InfoWindow>
        </Marker>
      );
    }

    return (
      <Marker
        key={uuid}
        position={{ lat, lng }}
        icon={markerIcon}
        onClick={this.handleMarkerClick(uuid)}
      />
    );
  };

  renderMarkers = () => {
    const { activeId, hotspots } = this.props;

    return hotspots.reduce((notActiveHotspots, hotspot) => {
      const isActive = activeId === hotspot.uuid;
      if (!isActive) {
        notActiveHotspots.push(this.renderMarker(hotspot));
      }

      return notActiveHotspots;
    }, []);
  };

  render() {
    const {
      activeHotspot = {},
      defaultCenter,
      onCenterChanged,
      onMapLoaded,
      onZoomChanged,
      zoom,
    } = this.props;

    const defaultOptions = {
      disableDefaultUI: true,
    };

    return (
      <GoogleMap
        zoom={zoom}
        defaultCenter={defaultCenter}
        ref={onMapLoaded}
        onZoomChanged={onZoomChanged}
        onCenterChanged={onCenterChanged}
        defaultOptions={defaultOptions}
      >
        <MarkerClusterer
          averageCenter
          enableRetinaIcons
          defaultImageExtension={'png'}
          defaultImagePath={'/img/map-cluster.png'}
          defaultMaxZoom={16}
          defaultStyles={[
            {
              url: '/img/map-cluster.png',
              height: 49,
              width: 49,
              anchorIcon: [-25, -25],
              textColor: '#00aaff',
              textSize: '12',
              lineHeight: 50,
              backgroundPosition: '0 1',
            },
          ]}
        >
          {this.renderMarkers()}
        </MarkerClusterer>
        {this.renderMarker(activeHotspot)}
      </GoogleMap>
    );
  }
}

export default enhance(MapComponent);
