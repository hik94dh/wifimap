export default {
  appStore: {
    storeName: 'App store',
    appName: 'WiFi Map - Get Free Internet',
    rating: 4,
    storeLink:
      'https://itunes.apple.com/app/apple-store/id548925969?pt=95834802&ct=website&mt=8',
  },
  googlePlay: {
    storeName: 'Google play',
    appName: 'WiFi Map - Free Internet Worldwide',
    rating: 4.3,
    storeLink:
      'https://play.google.com/store/apps/details?id=io.wifimap.wifimap&referrer=utm_source%3Dwebsite',
  },
};
