import React from 'react';
import cl from 'classnames';
import isMobile from '@src/utils/mobile';
import { formatNetworkSpeed, formatDate } from '@src/utils/formatters';
import { getPrevUrl } from '@src/utils/query-string';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';

import Loader from '@components/loader';
import AppCard from '../app-card';
import AccessCard from '../access-card';
import './style.scss';
import apps from './apps-config';
import { getStreetViewImgUrl } from "@utils/helpers";

const PointCard = props => {
  const { pointData, handleBackToList, location, loadingPhotos } = props;
  const tips = pointData.tips;
  const accessData = tips[tips.length - 1];
  const backUrl = getPrevUrl(location.pathname);
  const { photos } = pointData;
  const hasPhotos = photos && photos.length;
  const photosClassName = !loadingPhotos ? '' : 'point-card__photos--loading';

  const imgStreetViewSrc = getStreetViewImgUrl([pointData.lat, pointData.lng]);

  return (
    <div className="point-card">
      <BtnBack handleBackToList={handleBackToList} backUrl={backUrl} />
      <div className={cl('point-card__photos', photosClassName)}>
        {!loadingPhotos ? (
          <ul className="point-card__photos-list">
            <PhotosListItem
              src={hasPhotos ? photos[0] : imgStreetViewSrc}
              width="420"
              height="300"
            />
          </ul>
        ) : (
          <Loader />
        )}
      </div>
      <div className={'point-card__info'}>
        <ul className="point-card__info-list">
          <PlaceBlock data={pointData} />
          <WifiBlock data={pointData} />
          <AccessCard data={accessData} />
          <NetworkBlock data={pointData} />
          {isMobile.iOS() && <AppCard {...apps.appStore} />}
          {isMobile.Android() && <AppCard {...apps.googlePlay} />}
        </ul>
      </div>
    </div>
  );
};

const BtnBack = ({ handleBackToList, backUrl }) => (
  <Link className="point-card__back" onClick={handleBackToList} to={backUrl}>
    Back to list
  </Link>
);

const PhotosListItem = ({ src, alt }) => {
  return (
    <li className="point-card__photo">
      <img src={src} alt={alt} width={420} height={300} />
    </li>
  );
};

const InfoListItem = ({
  children,
  className,
  imgProps = {},
  imgSrc,
  imgWrapperClass,
}) => {
  return (
    <li className={cl('point-card__info-item', className)}>
      <div className={cl('point-card__info-img', imgWrapperClass)}>
        <img src={imgSrc} alt="" {...imgProps} />
      </div>
      <div
        className={cl('point-card__info-content', {
          'point-card__info-content--thumb': !imgSrc,
        })}
      >
        {children}
      </div>
    </li>
  );
};

// Information about place
// TODO: should can use dynamic place icon (imgSrc)
const PlaceBlock = ({ data }) => {
  const { address, category, name } = data;
  const imgSrc = `/img/category_icons/${category}.png`;

  return (
    <InfoListItem
      imgSrc={imgSrc}
      imgWrapperClass={cl('i', 'i--category')}
      imgProps={{
        width: 28,
        height: 28,
      }}
    >
      <div className="point-card__place-block">
        <div className="point-card__place-block-name">{name}</div>
        <div className="point-card__place-block-address ">{address}</div>
      </div>
    </InfoListItem>
  );
};

// Information about wifi
const WifiBlock = ({ data }) => {
  const { ssid, connections_count, last_connected_at } = data;
  let formattedLastConnectedAt = last_connected_at
    ? formatDate(last_connected_at, 'DD.MM.YYYY')
    : '';

  const imgSrc = '/img/point-card/ic_wifi-block.png';
  const accentInfo = {
    fontWeight: 'bold',
    color: '#303030',
  };

  return (
    <InfoListItem imgSrc={imgSrc}>
      <div className="point-card__wifi-block-name">{ssid}</div>
      {!!connections_count && (
        <div className="point-card__wifi-block-connections">
          <span style={accentInfo}>{connections_count}</span> total connections
        </div>
      )}
      {!!last_connected_at && (
        <div className="point-card__wifi-block-last-connect">
          <span style={accentInfo}>{formattedLastConnectedAt}</span> last
          connection
        </div>
      )}
    </InfoListItem>
  );
};

// Information about network
const NetworkBlock = ({ data }) => {
  const { ping_time, download_speed, upload_speed } = data;

  const formattedDownload = download_speed
    ? formatNetworkSpeed(download_speed)
    : '';
  const formattedUploadSpeed = upload_speed
    ? formatNetworkSpeed(upload_speed)
    : '';
  const formattedPingTime = ping_time ? `${ping_time} ms` : '';

  const imgSrc = '/img/point-card/ic_network-block.png';

  if (!ping_time && !formattedPingTime && !download_speed) {
    return null;
  }

  return (
    <InfoListItem imgSrc={imgSrc}>
      <div className="point-card__network-block">
        {ping_time && (
          <div className="point-card__network-block-param">
            <img
              className="point-card__network-block-param-icon"
              src="/img/point-card/ic_ping.png"
              alt="Ping"
            />
            <div className="point-card__network-block-param-value">
              {formattedPingTime}
            </div>
            <div className="point-card__network-block-param-measure">Ping</div>
          </div>
        )}
        {download_speed && (
          <div className="point-card__network-block-param">
            <img
              className="point-card__network-block-param-icon"
              src="/img/point-card/ic_download.png"
              alt="Download"
            />
            <div className="point-card__network-block-param-value">
              {formattedDownload}
            </div>
            <div className="point-card__network-block-param-measure">
              Download
            </div>
          </div>
        )}
        {upload_speed && (
          <div className="point-card__network-block-param">
            <img
              className="point-card__network-block-param-icon"
              src="/img/point-card/ic_upload.png"
              alt="Upload"
            />
            <div className="point-card__network-block-param-value">
              {formattedUploadSpeed}
            </div>
            <div className="point-card__network-block-param-measure">
              Upload
            </div>
          </div>
        )}
      </div>
    </InfoListItem>
  );
};

export default withRouter(PointCard);
