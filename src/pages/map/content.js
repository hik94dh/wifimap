import React, { Component } from 'react';
import cl from 'classnames';
import { Link } from 'react-router-dom';
import { path, pathOr } from 'ramda';
import { getLatLng } from '@utils/helpers';
import { getHotspotUuid, getPrevUrl } from '@src/utils/query-string';
import Loader from '@components/loader';
import { fetchHotspot } from '@queries/fetchHotspot';

import PointsList from './points-list';
import Map from './map';
import PointCard from './point-card';

class MapContainer extends Component {
  map = null;
  placesService = null;

  state = {
    ...MapContainer.settings,
    hotspot: null,
    center: null,
    loadingHotspotPhotos: false,
  };

  static settings = {
    zoom: 11,
    hotSpotZoom: 15,
  };

  static getDerivedStateFromProps(props) {
    if (!props.city) {
      return null;
    }

    return {
      center: getLatLng(props.city, {
        lat: 'center_lat',
        lng: 'center_lng',
      }),
    };
  }

  componentDidMount() {
    // MOUNT SEARCH RESULTS FROM HOME PAGE
    // As a result we can get a Hotspot or a City

    // Set Hotspot
    const hotspot = path(['location', 'state', 'hotspot'], this.props);
    if (hotspot) {
      return this.setState({
        hotspot: this.props.location.state.hotspot,
        center: getLatLng(hotspot),
        zoom: MapContainer.settings.hotSpotZoom,
      });
    }

    // Set City
    // without hotspot id but with city coords
    const city = pathOr({}, ['location', 'state', 'city'], this.props);
    if (city.lat & city.lng) {
      return this.setState({
        center: {
          lat: city.lat,
          lng: city.lng,
        },
      });
    }

    // SET HOTSPOT DIRECT FROM SEARCH QUERY
    const hotspotUuid = getHotspotUuid(this.props);
    if (hotspotUuid) {
      return this.activateHotspot(hotspotUuid);
    }
  }

  componentDidUpdate(prevProps) {
    // SET MAP CENTER FROM CITY CENTER BY DEFAULT
    if (prevProps.city.id !== this.props.city.id) {
      this.setState({
        center: getLatLng(this.props.city, {
          lat: 'center_lat',
          lng: 'center_lng',
        }),
      });
    }
  }

  onMapLoaded(ref) {
    this.map = ref;

    if (!this.map) {
      return null;
    }

    if (!this.placesService) {
      // eslint-disable-next-line
      this.placesService = new google.maps.places.PlacesService(
        document.createElement('div'),
      );
    }
  }

  /**
   * Get photos urls from places service
   * @param {String} placeId
   * @return {Promise<Array<String>> | null}
   */
  async getPhotosUrlByPlaceId(placeId) {
    const photoSize = {
      maxHeight: 300,
      maxWidth: 420,
    };

    this.setState({
      loadingHotspotPhotos: true,
    });

    const placeDetails = await this.getPlaceDetails(placeId);
    let photos = null;
    if (placeDetails.photos) {
      photos = placeDetails.photos.map(item => {
        return item.getUrl(photoSize);
      });
    }
    this.setState({
      loadingHotspotPhotos: false,
    });

    return photos;
  }

  async getPlaceDetails(placeId) {
    return new Promise((resolve, reject) => {
      if (!this.placesService) {
        this.setState(
          {
            loadingHotspotPhotos: false,
          },
          () => reject('placesService is not initialized'),
        );
      }
      if (!placeId) {
        this.setState(
          {
            loadingHotspotPhotos: false,
          },
          () => reject('there is no placeId'),
        );
        return;
      }
      this.placesService.getDetails({ placeId }, (result, status) => {
        // eslint-disable-next-line
        if (status !== google.maps.places.PlacesServiceStatus.OK) {
          console.error(status);
          return;
        }
        resolve(result);
      });
    });
  }

  activateHotspot = uuid => {
    const { client, history } = this.props;

    // Prevent when already active
    if (uuid === this.state.activeHotspotUuid) {
      return;
    }

    //Loading start
    this.setState({
      isLoadingPoint: true,
    });

    client
      .query({
        query: fetchHotspot,
        variables: {
          uuid,
        },
      })
      .then(async res => {
        const hotspot = path(['data', 'hotspot'], res);
        if (!hotspot) {
          return null;
        }

        const center = { lat: hotspot.lat, lng: hotspot.lng };

        let photos = null;
        if (this.placesService) {
          photos = await this.getPhotosUrlByPlaceId(
            hotspot.google_place_id,
          ).catch(err => console.log(err));
        }

        this.setState(
          {
            center,
            hotspot: { ...hotspot, photos },
            isLoadingPoint: false,
            zoom: MapContainer.settings.hotSpotZoom,
          },
          () => {
            this.map && this.map.panTo(center);
            history.push(`/cities/${hotspot.city.slug}/map/${hotspot.uuid}`);
          },
        );
      })
      .catch(() => {
        this.setState({
          isLoadingPoint: false,
        });
      });
  };

  handleZoomChanged() {
    const zoom = this.map.getZoom();
    if (zoom !== this.state.zoom) {
      this.setState({ zoom });
    }
  }

  deactivateHotspot = () => {
    this.setState({
      hotspot: null,
    });
  };

  render() {
    const {
      city: { hotspots_count, name },
      hotspotsCoords,
      hotspotsList,
      isLoadingList,
      isLoadingCoords,
      location: { pathname },
    } = this.props;

    const {
      center,
      hotspot,
      isLoadingPoint,
      loadingHotspotPhotos,
      zoom,
    } = this.state;
    const activeHotspotUuid = path(['uuid'], hotspot);

    const mapPlacesContent = activeHotspotUuid ? (
      <PointCard
        handleBackToList={this.deactivateHotspot}
        loadingPhotos={loadingHotspotPhotos}
        pointData={hotspot}
      />
    ) : (
      <PointsList
        activeHotspotUuid={activeHotspotUuid}
        activateHotspot={this.activateHotspot}
        hotspots={hotspotsList}
        hotspots_count={hotspots_count}
        realHotspots_count={hotspotsCoords.length}
        name={name}
        pathname={pathname}
      />
    );
    const isLoadingInfo = isLoadingList || isLoadingPoint;

    return (
      <div className={cl('map', 'act')}>
        <div className="map__container">
          <div className="map-places">
             {isLoadingInfo ? <Loader /> : mapPlacesContent}
          </div>
          {center && (
            <Map
              activeHotspot={hotspot}
              activeId={activeHotspotUuid}
              defaultCenter={center}
              deactivateHotspot={this.deactivateHotspot}
              handleMarkerClick={this.activateHotspot}
              hotspots={hotspotsCoords}
              isLoadingCoords={isLoadingCoords}
              onMapLoaded={ref => this.onMapLoaded(ref)}
              onZoomChanged={() => this.handleZoomChanged()}
              zoom={zoom}
            />
          )}
          <Link
            className="map__close js-close-map"
            to={getPrevUrl(pathname, activeHotspotUuid ? -2 : -1)}
          />
        </div>
      </div>
    );
  }
}

MapContainer.defaultProps = {
  hotspotsCoords: [],
  hotspotsList: [],
};

export default MapContainer;
