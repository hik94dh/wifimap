import withMapApollo from '@components/withMapApollo';
import { compose, withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';

import './styles.scss';
import Content from './content';

const enhance = compose(withApollo, withMapApollo, withRouter);

export default enhance(Content);
