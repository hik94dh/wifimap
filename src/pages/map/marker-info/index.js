import React from 'react';
import { generateCategoryIconUrl } from '@utils/helpers';
import './style.scss';

const MarkerInfo = ({category, name, address}) => {
  
  const imgSrc = generateCategoryIconUrl(category);
  
  return (
    <div className="marker-info">
      <div className="marker-info__category">
          <div className="i i--category">
              <img src={imgSrc} alt="" width="28" height="28" />
          </div>
      </div>
      <div className="marker-info__content">
        <div className="marker-info__name">
          { name || 'WIFI name' }
        </div>
        <div className="marker-info__address">
          { address || 'Place Address' }
        </div>
      </div>
    </div>
  )
};

export default MarkerInfo;