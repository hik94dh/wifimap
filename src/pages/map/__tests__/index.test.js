import React from 'react';
import graphql  from '../index';
import renderer from 'react-test-renderer';

it('MapContainer testing', () => {
    const tree = renderer.create(<graphql  />).toJSON();
    expect(tree).toMatchSnapshot();
});
