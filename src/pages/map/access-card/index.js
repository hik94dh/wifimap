import React, { Component } from 'react';
import { formatDate } from '@src/utils/formatters';

import './style.scss';

class AccessCard extends Component {
  static defaultProps = {
    data: {},
  };

  state = {
    isAvatarLoaded: false,
    isAvatarLoadError: false,
  };

  shouldComponentUpdate(nextProps) {
    if (nextProps.data.id === this.props.data.id) {
      return false;
    }
    return true
  }

  handleAvatarError = () => {
    this.setState({
      isAvatarLoadError: true,
    });
  };

  handleAvatarLoad = () => {
    this.setState({
      isAvatarLoaded: true,
    });
  };

  render() {
    const { isAvatarLoadError, isAvatarLoaded } = this.state;
    const {
      data: { password, owner_name, owner_avatar_url, created_at },
    } = this.props;
    const isOpenWifi = !password;
    const imgName = isOpenWifi ? 'ic_open-wifi' : 'ic_open-wifi'; // @TODO: change no open wifi img name
    const imgSrc = `/img/access-card/${imgName}.png`;
    const avatarImgSrc =
      isAvatarLoaded && !isAvatarLoadError
        ? owner_avatar_url
        : `/img/access-card/ic_avatar.png`;
    return (
      <div className="access-card">
        {
          <div className="access-card__content">
            <div className="access-card__heading">
              <div className="access-card__heading-img">
                <img src={imgSrc} alt="" />
              </div>
              <div className="access-card__heading-content">
                <div className="access-card__password">
                  {isOpenWifi ? 'Open Wi-Fi' : password}
                </div>
                {isOpenWifi && (
                  <div className="access-card__description">
                    No password required
                  </div>
                )}
              </div>
            </div>
            <div className="access-card__owner">
              <div className="access-card__owner-info">
                <div className="access-card__owner-avatar">
                  <img
                    src={avatarImgSrc}
                    onError={this.handleAvatarError}
                    onLoad={this.handleAvatarLoad}
                    alt=""
                  />
                </div>
                <div className="access-card__owner-name">
                  {owner_name || 'Anonymous'}
                </div>
              </div>
              <div className="access-card__owner-created-at">
                {formatDate(created_at, 'MMM DD, YYYY')}
              </div>
            </div>
          </div>
        }
      </div>
    );
  }
}

export default AccessCard;
