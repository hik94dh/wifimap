import React from 'react';
import splitNumber from '@utils/splitNumbers';
import { formatNetworkSpeed, formatDateFromNow } from '@src/utils/formatters';
import { getPrevUrl } from '@src/utils/query-string';
import { Link } from 'react-router-dom';

import './styles.scss';

const ListTitle = ({ hotspots_count, name, pathname }) => (
  <Link
    className="map-places__item map-places__item--main"
    to={getPrevUrl(pathname)}
  >
    <div className="map-places__descr">
      <div className="map-places__title">{name}</div>
      <div className="map-places__text">
        {splitNumber(hotspots_count)} Free WiFi
      </div>
    </div>
  </Link>
);

const ListItem = ({
  activateHotspot,
  address,
  category,
  download_speed,
  last_connected_at,
  name,
  pathname,
  ssid,
  uuid,
}) => {
  const formattedDownload = download_speed
    ? formatNetworkSpeed(download_speed)
    : '';
  const wifiOutdate = last_connected_at
    ? formatDateFromNow(last_connected_at)
    : '';

  const imgSrc = `/img/category_icons/${category}.png`;

  return (
    <a
      className="map-places-item"
      onClick={() => activateHotspot(uuid)}
    >
      <div className="map-places-item__img i i--category">
        <img src={imgSrc} alt="" width="28" height="28" />
      </div>
      <div className="map-places-item__content">
        <div className="map-places-item__place">
          <div className="map-places-item__place-name">{name}</div>
          <div className="map-places-item__place-address">{address}</div>
        </div>
        <div className="map-places-item__wifi">
          <div>
            <div className="map-places-item__wifi-name">{ssid}</div>
          </div>

          <div className="map-places-item__wifi-params">
            {download_speed && (
              <div className="map-places-item__meta map-places-item__meta--speed">
                {formattedDownload}
              </div>
            )}
            {wifiOutdate && (
              <div className="map-places-item__meta map-places-item__meta--date">
                {wifiOutdate}
              </div>
            )}
          </div>
        </div>
      </div>
    </a>
  );
};

const List = ({
  activeHotspotUuid,
  activateHotspot,
  hotspots,
  hotspots_count,
  name,
  pathname,
  realHotspots_count,
}) => (
  <ul className="map-places__list">
    <ListTitle
      hotspots_count={
        hotspots_count < 1000 ? realHotspots_count : hotspots_count
      }
      name={name}
      pathname={pathname}
    />
    {hotspots.map(hotspot => {
      return (
        <ListItem
          activateHotspot={activateHotspot}
          key={hotspot.uuid}
          pathname={pathname}
          {...hotspot}
        />
      );
    })}
  </ul>
);

export default List;
