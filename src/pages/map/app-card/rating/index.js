import React from 'react';
import './style.scss';


const Rating = (props) => {
  const { rating, countStars } = props;

  let starsNumber = [];

  for (let i = 1; i <= countStars; i++) {
    starsNumber.push(i);
  }

  const stars = starsNumber.map((number) => (
    <Star
      key={number}
      rating={rating}
      starNumber={number}
    />
  ))

  return (
    <div className="rating">
      {stars}
    </div>
  )
}

const Star = (props) => {
  const {
    starNumber,
    rating
  } = props;

  const ratingInteger = Math.floor(rating);

  let starModificator = ''

  if (starNumber <= ratingInteger) {
    starModificator = 'rating__star--full';
  } else if ((rating !== ratingInteger) && ((starNumber - 1) === ratingInteger)) {
    starModificator = 'rating__star--half'
  } else {
    starModificator = ''
  }

  return (
    <span className={`rating__star ${starModificator}`} />
  )

}

export default Rating;