import React from 'react';
import './style.scss';

import Rating from './rating';

const AppCard = (props) => {
  const {
    storeName,
    appName,
    rating,
    storeLink
  } = props;
  
  // WiFi Map - Free Internet Worldwide
  // 3.4
  
  return (
    <div className="app-card">
      <img className="app-card__icon" src="/img/app-card/ic_app-store.png" alt="Install app"/>
      <div className="app-card__store-info">
        <a className="app-card__link">{appName}</a>
        {`${storeName}: ${rating}`}
        <div className="app-card__rating">
          <Rating rating={rating} countStars={5} />
        </div>
      </div>
      <a className="app-card__btn-download" href={storeLink} >Install</a>
    </div>
  )
}

export default AppCard;