import React from 'react';

const Privacy = () =>
  <div>
    <p>
      WiFi Map – Privacy Policy<br/>
      May 23, 2018<br/>
    </p>
    <p>
    WiFi Map LLC (“WiFi Map” or “we”) collects information about you when you install and use our mobile application (“App”) and through other interactions and communications you have with us. This privacy policy (“Policy”) applies to persons anywhere in the world who use our App or otherwise interact or communicate with us (each such person, a “User” or “you”). If at any time you disagree with this Policy, you must refrain from installing our App, or, if you have already installed our App, refrain from using it and delete it. By installing or using our App you consent to our collection and use of information in accordance with this Policy.
    </p>
    <p>
    The primary function of our App is crowdsourcing from our Users information about WiFi hotspots and making that information available to our Users to help them find and access hotspots.
    </p>
    <p>
    Accordingly, we collect the geographical locations of hotspots, along with the names and addresses of the venues in which they are located. For each hotspot, we collect its router IP (remote IP) address, BSSID, SSID, password protection status and, as applicable, password. By uploading the password for a hotspot through our App, you represent and warrant to us that you are the owner of the hotspot, or have been authorized by the owner of the hotspot to upload the password through our App. If you are not the owner of a hotspot, and have not been authorized by the owner of that hotspot to upload the password through our App, you must not upload the password for that hotspot. We also collect information about WiFi signal strength, horizontal and vertical accuracy, speed test information, captive portal mode and the times at which your mobile device is connected to a particular hotspot.
    </p>
    <p>
    The hotspot data that we collect is made available to our other Users through our Apps.

    </p>
    <p>
    We also collect information about Users and their mobile devices and networks. When you install our App, and as you use our App, we collect (a) your email address, (b) your Google, Facebook, VK or other social networking profile, to the extent available, (c) your age range, (d) your country, language and time zone, (e) the make, model, operating system, resolution, device ID, advertisement ID and, if applicable, MAC address of your mobile device, (f) the name of your mobile carrier and speed test information about the performance of your mobile network and (g) the other applications installed on your mobile device. While our App is installed on your mobile device, whether or not you are using our App, we regularly track your precise geographical location. As you use our App, we collect your search queries, including cities and venues that you search for.

    </p>
    <p>
    All User payments for the pay version of our App are handled through the portal established by the publisher of the operating system for the User’s mobile device (iOS App Store, Google Play, etc.) and we do not receive, process or store Users’ credit card information or other User payment information.
    </p>
    <p>
    Most mobile platforms (iOS, Android, etc.) have defined certain types of device data that apps cannot access without your consent, and have various permission systems for obtaining your consent. The iOS platform will alert you the first time our App wants permission to access certain types of data and will let you consent (or not consent) to that request. Android devices will notify you of the permissions that our App seeks before you first use the App, and by using the App you consent to those permissions.
    </p>
    <p>
    We may use the information we collect to:
    <ul Style="list-style: circle; padding-left: 50px;">
    <li Style="list-style: circle;">provide, maintain and improve our services, develop new features and send product updates and administrative messages;
    </li>
    <li Style="list-style: circle;">perform internal operations, including troubleshooting, data analysis, testing and research; and
    </li>
    <li Style="list-style: circle;">send you, or recommend, advertisements and other communications we think will be of interest to you and otherwise personalize our services.
    </li>
    </ul>
    </p>
    <p>
    To opt out of our use of the personal information that we collect about you for advertising purposes, please contact us at support@wifimap.io with a request that we not use your personal information for advertising purposes, and we will honor that request.
    </p>
    <p>
    We do not knowingly collect or store personally identifiable information from anyone under the age of 13 unless or except as permitted by law. BY INSTALLING OR USING OUR APP, YOU REPRESENT AND WARRANT TO US THAT YOU ARE 13 YEARS OF AGE OR OLDER. If we are made aware that we have received personally identifiable information from someone under 13, we will use reasonable efforts to remove that information from our records.
    </p>
    <p>
    We may transfer the information we collect to, and process and store it in, the United States and other countries, some of which may have less protective data protection laws than the region in which you reside. We also may transfer information to vendors, consultants, marketing partners and other service providers who need access to that information to perform services on our behalf. By installing or using the App, you consent to those transfers of information to other countries and to service providers.
    </p>
    <p>
    We may share the information we collect, including WiFi hotspot information and user information and including personally identifiable information, with third parties under licensing or other arrangements. Those third parties may use that information for research and commercial purposes, including advertising and mobile network and hotspot performance testing. Without limiting the generality of this paragraph, additional information about our sharing of information with certain third-party partners are set out below.
    </p>
    <p>We also may share the information we collect:
    <ul Style="list-style: circle; padding-left: 50px;">
    <li Style="list-style: circle;">with our affiliates for development, data processing, storage and logistics purposes;
    </li>
    <li Style="list-style: circle;">in response to a request for information by a competent authority if we believe that disclosure is in accordance with, or required by, any applicable law, regulation or legal process;
    </li>
    <li Style="list-style: circle;">with law enforcement officials, governmental authorities or other third parties if we believe your actions are inconsistent with our terms of use or policies, or to protect the rights, property or safety of WiFi Map or others;
    </li>
    <li Style="list-style: circle;">in connection with, or during the negotiations of, any merger, sale of assets, consolidation, restructuring, financing or acquisition of all or a portion of our business;
    </li>
    <li Style="list-style: circle;">if we otherwise notify you and you consent to the sharing; and
    </li>
    <li Style="list-style: circle;">in an aggregated or anonymized form that cannot reasonably be used to identify you.
    </li>
    </ul>
    </p>
    <p>
    Additional details about our sharing of information with certain of our current third-party partners follow:
    <ul Style="list-style: circle; padding-left: 50px;">
      <li Style="list-style: circle;">
        **For US, England and France users only.
        <br/>
        In order to optimize advertising and bring out best possible offers we might share your Advertising ID, Wi-Fi Network Address (MAC Address) and location with Fidzup. All collected data will be automatically deleted after 1 year. For more information please visit their Privacy Policy: https://www.fidzup.com/en/privacy/
      </li>
    </ul>
    </p>
    <p>
    We may change this Policy from time to time by posting an updated version of this Policy on our website or through our App. Your continued use of our App after any such update will constitute your consent to the changes. We encourage you to periodically review this Policy for the latest information on our privacy practices.
    </p>
  </div>

export default Privacy;
