import api from '@src/api';
import { path } from 'ramda';

export function generateCategoryIconUrl(category) {
  return `/img/category_icons/${category}.png`;
}

/**
 * Get url for load image from streetview
 * @param {Array} size [width, height]
 * @param {Array} location [lat, lng]
 * @return {String} url for using in image src.
 */
export function getStreetViewImgUrl(location, size = [420, 300]) {
  // TODO change key to correct with activated streetview.
  const baseUrl = `https://maps.googleapis.com/maps/api/streetview?key=${api.map}`;
  const locationParam = `location=${location[0]},${location[1]}`;
  const sizeParam = `size=${size[0]}x${size[1]}`;
  return baseUrl + '&' + locationParam + '&' + sizeParam;
}

export function areCoordsEqual(props, otherProps) {
  return (
    path(['city', 'center_lat'], props) ===
      path(['city', 'center_lat'], otherProps) &&
    path(['city', 'center_lng'], props) ===
      path(['city', 'center_lng'], otherProps)
  );
}

export function getLatLng(source, alias = { lat: 'lat', lng: 'lng' }) {
  return { lat: source[alias.lat], lng: source[alias.lng] };
}
