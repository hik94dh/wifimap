import moment from 'moment';


// init moment
const locale = navigator.language;
moment.locale(locale);


export {
  formatNetworkSpeed,
  formatDate,
  formatDateFromNow
}


/**
 * Formatted network speed from kBytes
 * @param {String} speed
 * @return {String} formatted like '9.85 mb/s'
 */
function  formatNetworkSpeed(speed) {
  return `${Math.floor(speed * 100) / 100} mb/s`
}


/**
 * Format date
 * @param {Number} date in seconds
 * @return {String} formatted
 */
function formatDate(date, format) {
  return moment(date * 1000).format(format)
}


/**
 * Format date from now
 * @param {Number} date in seconds
 * @return {String} formatted like '4 days ago' and other same formats
 */
function formatDateFromNow(date) {
  return moment(date * 1000).fromNow()
}