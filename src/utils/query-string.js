import { path } from 'ramda';

export const getCityId = pathname => pathname.split('-')[0];

export const getPrevUrl = (pathname, nesting = -1) =>
  pathname
    .split('/')
    .slice(0, nesting)
    .join('/');

export const redirectToCorrectUrl = (pathname, id = null) => {
  let arr = pathname.split('/');
  let last = arr.length - 1;

  if (arr[last] === '') {
    arr = arr.slice(0, -1);
    last -= 1;
  }

  if (arr[last] === 'map') {
    if (id) {
      arr.push(id);
    }
    return arr.join('/');
  }

  if (arr[last] !== 'map') {
    arr = arr.slice(0, -1);
    if (id) {
      arr.push(id);
    }
    return arr.join('/');
  }
};

export const getHotspotUuid = source =>
  path(['match', 'params', 'hotspotUuid'], source);
