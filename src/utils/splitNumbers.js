export default function splitNumber(number = 0, divider = ',') {
  return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
}

export function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}
