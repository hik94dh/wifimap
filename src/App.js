import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect as LocalRedirect,
} from 'react-router-dom';
import { ApolloProvider } from 'react-apollo';
import apolloClient from './apollo-client';
import HttpsRedirect from 'react-https-redirect';

import Home from './pages/home';
import Contacts from './pages/contacts';
import Countries from './pages/countries';
import Country from './pages/country';
import City from './pages/city';
import Privacy from './pages/privacy';
import Map from './pages/map';
import Terms from './pages/terms';
import NotFound from './pages/not_found';
import Header from './components/header/';
import Footer from './components/footer';
import GAListener from './components/ga_listener';
import ScrollToTop from './components/scroll_to_top';
import withJquery from './withJquery';
import PrivacyAndroid from './pages/privacy_android';
import Redirect from '@components/redirect';

const App = () => (
  <ApolloProvider client={apolloClient}>
    <HttpsRedirect>
      <Router>
        <GAListener>
          <ScrollToTop>
            {/* Header */}
            <Switch>
              <Route exact path="/" render={() => <Header light />} />
              <Route
                exact
                path="/countries/:id"
                render={() => <Header light />}
                light
              />
              <Route exact path="/cities/:id" render={() => <Header light />} />
              <Route exact path="/privacy" />
              <Route exact path="/privacy_android" />
              <Route exact path="/terms" />
              <Route exact path="/cities/:id/map/:hotspotId" />
              <Route component={Header} />
            </Switch>
            {/* Pages */}
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/contacts" component={Contacts} />
              <Route exact path="/countries" component={Countries} />
              <Route exact path="/countries/:id" component={Country} />
              <Route exact path="/cities/:id" component={City} />
              <Route exact path="/cities/:id/map/" component={Map} />
              <Route path="/cities/:id/map/:hotspotId" component={Map} />
              <Route exact path="/map" component={Map} />
              <Route path="/cities/:id/map/:hotspotId" component={Map} />

              <Route exact path="/privacy" component={Privacy} />
              <Route exact path="/privacy_android" component={PrivacyAndroid} />
              <Route exact path="/terms" component={Terms} />
              <Route
                exact
                path="/password_reset"
                render={() => (
                  <Redirect to="https://api.wifimap.io/password_reset" />
                )}
              />
              <Route
                exact
                path="/help"
                render={() => <Redirect to="https://wifimap.zendesk.com" />}
              />
              <Route
                exact
                path="/blog"
                render={() => <Redirect to="https://medium.com/wifi-map" />}
              />
              <Route
                exact
                path="/ios"
                render={() => <Redirect to="https://itunes.apple.com/app/apple-store/id548925969?pt=95834802&ct=website&mt=8" />}
              />
              <Route
                exact
                path="/android"
                render={() => <Redirect to="https://play.google.com/store/apps/details?id=io.wifimap.wifimap&referrer=utm_source%3Dwebsite" />}
              />
              <LocalRedirect from="/get" to="/" />
              <Route component={NotFound} />
            </Switch>
            {/* Footer */}
            <Switch>
              <Route exact path="/privacy" />
              <Route exact path="/privacy_android" />
              <Route exact path="/terms" />
              <Route exact path="/cities/:id/map/:hotspotId" />
              <Route component={Footer} />
            </Switch>
          </ScrollToTop>
        </GAListener>
      </Router>
    </HttpsRedirect>
  </ApolloProvider>
);

export default withJquery(App);
