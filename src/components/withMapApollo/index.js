import { compose, graphql } from 'react-apollo';
import { fetchHotspotsCoords, fetchHotspotsList } from '@queries/fetchHotspot';
import { getCityId } from '@src/utils/query-string';

export default compose(
  graphql(fetchHotspotsCoords, {
    options: ({ match: { params: { id } } }) => ({
      variables: { id: getCityId(id), all_hotspots: '' },
    }),
    skip: ({ match: { params: { id } } }) => !id,
    props: ({ data: { city = {}, loading } }) => ({
      hotspotsCoords: city.hotspots,
      isLoadingCoords: loading,
    }),
  }),
  graphql(fetchHotspotsList, {
    options: ({ match: { params: { id } } }) => ({
      variables: { id: getCityId(id) },
    }),
    skip: ({ match: { params: { id } } }) => !id,
    props: ({ data: { city: { hotspots, ...otherProps } = {}, loading } }) => ({
      // TODO: wait for API improvements
      // TODO: number of hotspots should to be returned
      hotspotsList: hotspots,
      isLoadingList: loading,
      city: {
        ...otherProps,
      },
    }),
  }),
);
