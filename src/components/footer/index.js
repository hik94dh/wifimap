import React from 'react';
import { Link } from 'react-router-dom';

export default () => (
  <div className="content-block">
    <footer id="footer" className="footer">
      <div className="wrap">
        <div className="footer__inner">
          <div className="parts-row-table parts-6 parts-collapse-lg">
            <div className="col-item">
              <div className="footer__content footer__content--left">
                <Link to="/">
                  <img src="/img/logo.png" alt="" className="logo" />
                </Link>
              </div>
            </div>
            <div className="col-item part-6x4">
              <div className="footer__content footer__content--center">
                <nav className="nav-list">
                  <ul>
                    <li className="nav-list__item">
                      <Link to="/" className="nav-list__link">
                        Home
                      </Link>
                    </li>
                    <li className="nav-list__item">
                      <Link to="/contacts" className="nav-list__link">
                        Contact Us
                      </Link>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
            <div className="col-item">
              <div className="footer__content footer__content--right">
                <div className="parts-row parts-divide parts-lg-4">
                  <div className="col-item">
                    <a
                      href="https://twitter.com/wifimapapp"
                      className="footer__socials-item"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <span className="fa fa-twitter" />
                    </a>
                  </div>
                  <div className="col-item">
                    <a
                      href="https://www.facebook.com/wifimap.io/"
                      className="footer__socials-item"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <span className="fa fa-facebook" />
                    </a>
                  </div>
                  <div className="col-item">
                    <a
                      href="https://plus.google.com/+Wifiofflinemap"
                      className="footer__socials-item"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <span className="fa fa-google-plus" />
                    </a>
                  </div>
                  <div className="col-item">
                    <a
                      href="https://www.instagram.com/wifimap/"
                      className="footer__socials-item"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <span className="fa fa-instagram" />
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="footer__copyright">
          <div className="parts-row-table parts-2 parts-collapse-lg">
            <div className="col-item">
              <div className="footer__copyright-text">
                Our goal is to make internet more accessible for people
                worldwide. <br />America, Europe, Middle East, Asia, Australia,
                Africa.
              </div>
            </div>
            <div className="col-item">
              <div className="footer__copyright-links">
                <div className="parts-row parts-divide-md">
                  <div className="col-item">
                    <Link to="/privacy" className="footer__copyright-link">
                      Privacy Policy
                    </Link>
                  </div>
                  <div className="col-item">
                    <Link to="/terms" className="footer__copyright-link">
                      Terms and Condition
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </div>
);
