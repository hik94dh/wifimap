import React, { Component } from 'react';
import $ from 'jquery';

/* eslint-disable */
export default function withJquery(WrappedComponent) {
  return class extends Component {
    componentDidMount() {
      // function header scroll hight
      function headerShift() {
        var $header = $('.header'),
          $window = $(window);
        if ($window.scrollTop() > 0) {
          $header.addClass('header--shift');
        } else {
          $header.removeClass('header--shift');
        }
      }
      headerShift();

      // window scroll events
      $(window).scroll(function() {
        headerShift();
      });
    }
    render() {
      return <WrappedComponent {...this.props} />;
    }
  };
}
/* eslint-enable */
