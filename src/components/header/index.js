import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import cl from 'classnames';
import withJquery from './withJquery';

class Header extends Component {
  state = {
    isOpen: false,
  };

  componentDidUpdate(prevProps, prevState) {
    const body = document.body;
    const className = 'menu-open';
    if (prevState.isOpen === false && this.state.isOpen === true) {
      body.classList.add(className);
    } else {
      body.classList.remove(className);
    }
  }

  handleToggleOpen = () =>
    this.setState(prevState => ({
      isOpen: !prevState.isOpen,
    }));

  render() {
    const { isOpen } = this.state;
    const { light } = this.props;
    return (
      <header
        className={cl('header', {
          'header--white': light,
        })}
      >
        <div className="wrap">
          <div className="header__inner">
            <div className="header__left-nav">
              <Link to="/">
                <img
                  src="/img/logo.png"
                  srcSet="/img/logo@2x.png 2x"
                  alt=""
                  className="logo"
                />
                {light && (
                  <img
                    src="/img/logo-white.png"
                    srcSet="/img/logo-white@2x.png 2x"
                    alt=""
                    className="logo logo--white"
                  />
                )}
              </Link>
            </div>
            <div className="header__right-nav">
              <ul className="main-nav">
                <li className="main-nav__list">
                  <ul>
                    <li className="main-nav__list-item">
                      <NavLink
                        exact
                        to="/"
                        className="main-nav__item"
                        activeClassName="act"
                      >
                        Home
                      </NavLink>
                    </li>
                    <li className="main-nav__list-item">
                      <NavLink
                        to="/blog"
                        className="main-nav__item"
                        activeClassName="act"
                      >
                        Blog
                      </NavLink>
                    </li>
                    <li className="main-nav__list-item">
                      <NavLink
                        to="/help"
                        className="main-nav__item"
                        activeClassName="act"
                      >
                        Help
                      </NavLink>
                    </li>
                    <li className="main-nav__list-item">
                      <NavLink
                        to="/contacts"
                        className="main-nav__item"
                        activeClassName="act"
                      >
                        Contact Us
                      </NavLink>
                    </li>
                    <li className="main-nav__list-item">
                      <a
                        href="https://itunes.apple.com/app/apple-store/id548925969?pt=95834802&ct=website&mt=8"
                        className="main-nav__item main-nav__item--app"
                      >
                        <img
                          src="/img/appstore-sm.png"
                          srcSet="/img/appstore-sm@2x.png 2x"
                          alt=""
                        />
                      </a>
                      <a
                        href="https://play.google.com/store/apps/details?id=io.wifimap.wifimap&referrer=utm_source%3Dwebsite"
                        className="main-nav__item main-nav__item--app"
                      >
                        <img
                          src="/img/googleplay-sm.png"
                          srcSet="/img/googleplay-sm@2x.png 2x"
                          alt=""
                        />
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
            <button
              className={cl('menu-btn', {
                act: isOpen,
              })}
              onClick={this.handleToggleOpen}
            >
              <div className="menu-btn__container">
                <span className="menu-btn__line menu-btn__line--line1" />
                <span className="menu-btn__line menu-btn__line--line2" />
                <span className="menu-btn__line menu-btn__line--line3" />
              </div>
            </button>
          </div>
        </div>
      </header>
    );
  }
}

export default withJquery(Header);
