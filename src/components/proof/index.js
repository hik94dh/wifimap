import React from 'react';

export default () => (
  <div className="section section--gray">
    <div className="wrap">
      <div className="section-title">
        <div className="section-title__title">
          Mobile Open WiFi on your phone
        </div>
        <div className="section-title__text section-title__text--gray">
          Continuously going from one place to another? Because of a large
          curated and crowdsourced collection of 70 million Internet hotspots,
          WiFi Map is building the largest worldwide Open Wireless Internet
          Community and makes sure you have access to a large number of free
          hotspots in New York.
        </div>
      </div>
      <div className="screens-list">
        <div className="parts-row parts-4 parts-divide-lg parts-hg-2 parts-collapse-lg">
          <div className="col-item">
            <div className="screens-list__item">
              <div className="screens-list__mockup">
                <img src="/img/f-img4.png" alt="" />
              </div>
              <div className="screens-list__image">
                <img src="/img/f-img4.png" alt="" />
              </div>
            </div>
          </div>
          <div className="col-item">
            <div className="screens-list__item act">
              <div className="screens-list__mockup">
                <img
                  src="/img/device-mockup.png"
                  srcSet="/img/device-mockup@2x.png 2x"
                  alt=""
                />
              </div>
              <div className="screens-list__image">
                <img src="/img/f-img2.png" alt="" />
              </div>
            </div>
          </div>
          <div className="col-item">
            <div className="screens-list__item">
              <div className="screens-list__mockup">
                <img src="/img/f-img3.png" alt="" />
              </div>
              <div className="screens-list__image">
                <img src="/img/f-img3.png" alt="" />
              </div>
            </div>
          </div>
          <div className="col-item">
            <div className="screens-list__item">
              <div className="screens-list__mockup">
                <img
                  src="/img/device-mockup.png"
                  srcSet="/img/device-mockup@2x.png 2x"
                  alt=""
                />
              </div>
              <div className="screens-list__image">
                <img
                  src="/img/app-screen-sm4.jpg"
                  srcSet="/img/app-screen-sm4@2x.jpg 2x"
                  alt=""
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);
