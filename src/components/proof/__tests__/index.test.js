import React from 'react';
import index from '../index';
import renderer from 'react-test-renderer';

it('proof testing', () => {
    const tree = renderer.create(<index />).toJSON();
    expect(tree).toMatchSnapshot();
});
