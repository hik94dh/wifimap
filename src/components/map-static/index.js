import React from 'react';
import queryString from 'querystring';
import propTypes from 'prop-types';
import api from '@src/api';

const MapStatic = ({ center, height, scale, width, zoom }) => {
  const baseUrl = 'https://maps.googleapis.com/maps/api/staticmap';
  const params = {
    center: `${center.lat}, ${center.lng}`,
    size: `${width}x${height}`,
    scale,
    zoom,
    key: api.map,
  };
  const src = `${baseUrl}?${queryString.stringify(params)}`;

  return <img src={src} alt=""/>;
};

MapStatic.defaultProps = {
  zoom: 13,
  scale: 2,
};

MapStatic.propTypes = {
  center: propTypes.shape({
    lat: propTypes.number.isRequired,
    lng: propTypes.number.isRequired,
  }),
  zoom: propTypes.oneOfType([propTypes.number, propTypes.string]),
  scale: propTypes.oneOfType([propTypes.number, propTypes.string]),
};

export default MapStatic;
