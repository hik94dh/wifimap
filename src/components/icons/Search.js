import React from 'react';

const Search = props => (
  <svg width={23} height={23} {...props}>
    <path
      d="M10.222 0A10.222 10.222 0 1 1 0 10.222 10.222 10.222 0 0 1 10.222 0zm0 2.555a7.667 7.667 0 1 1-7.666 7.667 7.667 7.667 0 0 1 7.666-7.667zm7.206 13.132l5.2 5.2a1.226 1.226 0 0 1-1.734 1.735l-5.2-5.2a1.226 1.226 0 0 1 1.734-1.735z"
      fill="#ffc800"
      fillRule="evenodd"
    />
  </svg>
);

export default Search;
