import React from 'react';

const Place = ({ color, ...restProps }) => (
  <svg xmlns="http://www.w3.org/2000/svg" width={18} height={22} {...restProps}>
    <path
      d="M8.987 22l-6.381-6.465a9.164 9.164 0 0 1 0-12.828l.044-.045a8.877 8.877 0 0 1 12.671 0l.044.043a9.167 9.167 0 0 1 0 12.832zm0-19.986a6.894 6.894 0 0 0-4.923 2.051l-.042.043a7.044 7.044 0 0 0 0 9.913l4.966 4.994 4.967-4.994a7.047 7.047 0 0 0 0-9.913l-.044-.043a6.894 6.894 0 0 0-4.924-2.053zM9 11.991a3 3 0 1 1 3.009-3 3.006 3.006 0 0 1-3.009 3zm0-4.282a1.285 1.285 0 1 0 1.285 1.285A1.286 1.286 0 0 0 9 7.709z"
      fill={color}
      fillRule="evenodd"
    />
  </svg>
);

Place.defaultProps = {
  color: '#c3c7cb',
};

export default Place;
