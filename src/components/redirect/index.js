import React from 'react';
import propTypes from 'prop-types';
import './styles.scss';

const Redirect = ({ to }) => {
  window.location.replace(to);
  return (
    <div className="page">
      <div className="section">
        <div className="wrap wrap--loading">Loading...</div>
      </div>
    </div>
  );
};

Redirect.propTypes = {
  to: propTypes.string.isRequired,
};

export default Redirect;
