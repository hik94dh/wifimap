import React, { Component, Fragment } from 'react';
import Script from 'react-load-script';
import api from '@src/api';

export default function withScript(WrappedComponent, url) {
  class withScript extends Component {
    state = {
      scriptLoaded: false,
    };

    componentWillUnmount() {
      if (document.querySelector('#google-places-api')) {
        document.querySelector('#google-places-api').remove();
      }
    }

    get mapUrl() {
      return `https://maps.googleapis.com/maps/api/js?key=${api.map}&v=3.exp&libraries=places`;
    }
    handleScriptLoad = () => {
      this.setState({
        scriptLoaded: true,
      });
    };

    render() {
      return (
        <Fragment>
          <Script
            url={this.mapUrl}
            onLoad={this.handleScriptLoad.bind(this)}
            attributes={{ id: 'google-places-api' }}
          />
          {this.state.scriptLoaded && (
            <WrappedComponent {...this.props} {...this.state} />
          )}
        </Fragment>
      );
    }
  }
  return withScript;
}
