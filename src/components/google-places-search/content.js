import React from 'react';
import PlacesAutocomplete, {
  geocodeByAddress,
} from 'react-places-autocomplete';
import IconSearch from '@components/icons/Search';
import IconPlace from '@components/icons/Place';
import { fetchHotspot } from '@queries/fetchHotspot';
import { pathOr } from 'ramda';
import fetchCity from '@queries/fetchCity';
import { withApollo } from 'react-apollo';

import './styles.scss';

class LocationSearchInput extends React.Component {
  state = {
    address: '',
    currentLocation: {},
    isLoadingPoint: false,
    isVisibleNoHotspotMsg: false,
  };

  static defaultSearchOptions = {
    radius: 2000,
  };

  componentDidMount() {
    // Turn off geolocation
    // if (navigator.geolocation) {
    //   navigator.geolocation.getCurrentPosition(position =>
    //     this.setState({
    //       currentLocation: {
    //         lat: position.coords.latitude,
    //         lng: position.coords.longitude,
    //       },
    //     }),
    //   );
    // }
  }

  handleChange = value => {
    this.setState({ address: value, isError: false, isVisibleNoHotspotMsg: false });
  };

  onError = (status, clearSuggestions) => {
    this.setState(
      {
        isError: true,
      },
      clearSuggestions,
    );
  };

  handleSelect = (address, placeId) => {
    const { client } = this.props;
    if (!placeId) {
      return this.setState({
        hotspotError: 'There is no such hotspot',
      });
    }
    this.setState(
      {
        isLoadingPoint: true,
      },
      () =>
        client
          .query({
            query: fetchHotspot,
            variables: {
              google_place_id: placeId,
            },
          })
          .then(res => {
            const hotspot = pathOr(null, ['data', 'hotspot'], res);
            const uuid = pathOr(null, ['data', 'hotspot', 'uuid'], res);
            const cityName = pathOr(
              null,
              ['data', 'hotspot', 'city', 'slug'],
              res,
            );
            if (uuid && cityName) {
              this.setState(
                {
                  isLoadingPoint: false,
                },
                () =>
                  history.push(`/cities/${cityName}/map/${uuid}`, { hotspot }),
              );
            } else {
              geocodeByAddress(address).then(res => {
                const city = pathOr([], [0, 'address_components'], res).find(
                  component =>
                    component.types.includes('locality') ||
                    component.types.includes('administrative_area_level_2'),
                );
                const lat = res[0].geometry.location.lat();
                const lng = res[0].geometry.location.lng();
                // get city
                if (!city) {
                  return this.setState({
                    cityError: 'There is no such city',
                    isVisibleNoHotspotMsg: true
                  });
                }
                geocodeByAddress(city.long_name).then(res => {
                  return client
                    .query({
                      query: fetchCity,
                      variables: { google_place_id: res[0].place_id },
                    })
                    .then(cityRes => {
                      if (!cityRes.data.city) {
                        return this.setState({
                          cityError: 'There is no such city',
                          isVisibleNoHotspotMsg: true
                        });
                      }
                      return history.push(
                        `/cities/${cityRes.data.city.slug}/map`,
                        { city: { lat, lng } },
                      );
                    });
                });
              });
            }
          }),
    );

    const { history } = this.props;
  };

  getSearchOptions = () => {
    const { currentLocation } = this.state;
    if (!currentLocation.lat && !currentLocation.lng) {
      return null;
    }
    return {
      // eslint-disable-next-line
      location: new google.maps.LatLng(
        currentLocation.lat,
        currentLocation.lng,
      ),
      ...LocationSearchInput.defaultSearchOptions,
    };
  };

  render() {
    const { className } = this.props;
    const { address, isError, isVisibleNoHotspotMsg } = this.state;
    return (
      <PlacesAutocomplete
        value={this.state.address}
        onSelect={this.handleSelect}
        onChange={this.handleChange}
        onError={this.onError}
        debounce={400}
        searchOptions={this.getSearchOptions()}
        highlightFirstSuggestion={true}
      >
        {({ getInputProps, suggestions, getSuggestionItemProps }) => (
          <div className={className}>
            <div className="search">
              <IconSearch />
              <input
                {...getInputProps({
                  autoComplete: 'off',
                  autoCorrect: 'off',
                  spellCheck: false,
                  className: 'search__input',
                  placeholder: 'Search by WiFi hotspots',
                })}
              />
            </div>
            {
              isVisibleNoHotspotMsg ?
                (<div className="search__result">
                <div className="search__result-item search__result-item--error">
                  <strong>Sorry! We could not find points for selected place.</strong>
                </div>
              </div>) : ''
            }
            <div className="search__result">
              {!isError ? (
                suggestions.map(suggestion => {
                  const {
                    formattedSuggestion: { mainText, secondaryText },
                  } = suggestion;

                  return (
                    <div
                      {...getSuggestionItemProps(suggestion, {
                        className: 'search__result-item',
                      })}
                    >
                      <IconPlace />
                      <div className="panel">
                        <div className="panel__title">{mainText} </div>
                        <div className="panel__desc">{secondaryText}</div>
                      </div>
                    </div>
                  );
                })
              ) : (
                <div className="search__result-item search__result-item--error">
                  <div className="panel">
                    <div className="panel__title">
                      Your search - <strong>{address}</strong> - did not match
                      any places
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
        )}
      </PlacesAutocomplete>
    );
  }
}

export default withApollo(LocationSearchInput);
