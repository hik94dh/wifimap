import { compose, withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';

import './styles.scss';
import Content from './content';
import withScript from './withScript';

const enhance = compose(withApollo, withRouter, withScript);

export default enhance(Content);
