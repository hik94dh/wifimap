import React from 'react';
import OwlCarousel from 'react-owl-carousel';

export default () => {
  const settings = {
    items: 8,
    dots: false,
    nav: true,
    margin: 10,
    smartSpeed: 500,
    loop: true,
    responsive: {
      0: {
        items: 2,
      },
      500: {
        items: 4,
      },
      700: {
        items: 6,
      },
      1000: {
        items: 8,
      },
    },
  };
  return (
    <div className="partners">
      <div className="wrap">
        <OwlCarousel
          className="partners__slider"
          {...settings}
        >
          <a
            href="http://edition.cnn.com/travel/article/best-travel-apps/index.html"
            target="_blank"
            rel="noopener noreferrer"
            className="partners__item"
          >
            <span className="partners__item-image">
              <img
                src="/img/partners/cnn.png"
                alt=""
                srcSet="/img/partners/cnn@2x.png 2x"
              />
            </span>
          </a>
          <a
            href="https://www.wsj.com/articles/do-you-really-need-a-wireless-phone-carrier-1425077991"
            target="_blank"
            rel="noopener noreferrer"
            className="partners__item"
          >
            <span className="partners__item-image">
              <img
                src="/img/partners/1.png"
                alt=""
                srcSet="/img/partners/1@2x.png 2x"
              />
            </span>
          </a>
          <a
            href="http://lifehacker.com/wifi-map-is-a-crowdsourced-list-of-routers-and-password-1681442571"
            target="_blank"
            rel="noopener noreferrer"
            className="partners__item"
          >
            <span className="partners__item-image">
              <img
                src="/img/partners/2.png"
                alt=""
                srcSet="/img/partners/2@2x.png 2x"
              />
            </span>
          </a>
          <a
            href="https://www.esquire.com/lifestyle/g22677427/best-travel-apps/"
            target="_blank"
            rel="noopener noreferrer"
            className="partners__item"
          >
            <span className="partners__item-image">
              <img
                src="/img/partners/esquire.png"
                alt="Esquire"
              />
            </span>
          </a>
          <a
            href="https://gigaom.com/2015/01/21/need-to-find-a-hotspot-wifi-map-has-2m-of-them-for-you/"
            target="_blank"
            rel="noopener noreferrer"
            className="partners__item"
          >
            <span className="partners__item-image">
              <img
                src="/img/partners/4.png"
                alt=""
                srcSet="/img/partners/4@2x.png 2x"
              />
            </span>
          </a>
          <a
            href="https://www.forbes.com/sites/debtfreeguys/2018/08/05/5-simple-steps-to-save-on-international-phone-calls-and-data/#6f89b8883864"
            target="_blank"
            rel="noopener noreferrer"
            className="partners__item"
          >
            <span className="partners__item-image">
              <img
                src="/img/partners/forbes.png"
                alt="forbes"
              />
            </span>
          </a>
          <a
            href="https://www.thisisinsider.com/best-travel-apps-in-2017-2017-8"
            rel="noopener noreferrer"
            target="_blank"
            className="partners__item"
          >
            <span className="partners__item-image">
              <img
                src="/img/partners/insider.png"
                alt="insider"
              />
            </span>
          </a>
          <a
            href="http://www.producthunt.com/posts/wifi-map"
            rel="noopener noreferrer"
            target="_blank"
            className="partners__item"
          >
            <span className="partners__item-image">
              <img
                src="/img/partners/7.png"
                alt=""
                srcSet="/img/partners/7@2x.png 2x"
              />
            </span>
          </a>
          <a
            href="https://www.netted.net/2015/02/18/wifi-map/"
            rel="noopener noreferrer"
            target="_blank"
            className="partners__item"
          >
            <span className="partners__item-image">
              <img
                src="/img/partners/5.png"
                alt=""
                srcSet="/img/partners/5@2x.png 2x"
              />
            </span>
          </a>
          <a
            href="http://www.ynet.co.il/articles/0,7340,L-4613744,00.html"
            rel="noopener noreferrer"
            target="_blank"
            className="partners__item"
          >
            <span className="partners__item-image">
              <img
                src="/img/partners/8.png"
                alt=""
                srcSet="/img/partners/8@2x.png 2x"
              />
            </span>
          </a>
          <a
            href="http://www.redmondpie.com/this-ios-android-app-features-millions-of-wifi-hotspots-and-passwords-for-free-internet-access/"
            target="_blank"
            rel="noopener noreferrer"
            className="partners__item"
          >
            <span className="partners__item-image">
              <img
                src="/img/partners/9.png"
                alt=""
                srcSet="/img/partners/9@2x.png 2x"
              />
            </span>
          </a>
          <a
            href="https://tech.co/wifi-map-7-million-users-2015-01"
            rel="noopener noreferrer"
            target="_blank"
            className="partners__item"
          >
            <span className="partners__item-image">
              <img
                src="/img/partners/10.png"
                alt=""
                srcSet="/img/partners/10@2x.png 2x"
              />
            </span>
          </a>
          <a
            href="https://www.popsci.com/find-free-wifi-hotspots#page-2"
            rel="noopener noreferrer"
            target="_blank"
            className="partners__item"
          >
            <span className="partners__item-image">
              <img
                src="/img/partners/popsci.jpg"
                alt="popsci"
              />
            </span>
          </a>
          <a
            href="https://www.traveldailynews.com/post/7-must-have-apps-for-travel"
            rel="noopener noreferrer"
            target="_blank"
            className="partners__item"
          >
            <span className="partners__item-image">
              <img
                src="/img/partners/traveldailynews.png"
                alt="traveldailynews"
              />
            </span>
          </a>
          <a
            href="https://www.guidingtech.com/wi-fi-hotspot-apps-android/"
            rel="noopener noreferrer"
            target="_blank"
            className="partners__item"
          >
            <span className="partners__item-image">
              <img
                src="/img/partners/guidingtech.png"
                alt="guidingtech"
              />
            </span>
          </a>
          <a
            href="https://download.cnet.com/WiFi-Map-Free-Passwords/3000-20420_4-76618659.html"
            rel="noopener noreferrer"
            target="_blank"
            className="partners__item"
          >
            <span className="partners__item-image">
              <img
                src="/img/partners/cnet.png"
                alt="cnet"
              />
            </span>
          </a>
        </OwlCarousel>
      </div>
    </div>
  );
};
