import React from 'react';
import SliderPartners from '../slider-partners';

export default () => (
  <div className="content-block">
    <div className="note-panel">
      <div className="wrap">
        <div className="note-panel__inner">
          <div className="parts-row parts-5 parts-collapse-lg">
            <div className="col-item part-5x4">
              <div className="note-panel__descr">
                <div className="note-panel__title">
                  <span>USU</span> - Use Share Update
                </div>
                <div className="note-panel__text">
                  <p>
                    If you love Free WiFi just as much as we do and want to help
                    others with finding Free and Reliable WiFi, then you are in
                    the right place. Real people add millions of new WiFis and
                    so can you!
                  </p>
                </div>
              </div>
            </div>
            <div className="col-item">
              <div className="note-panel__links">
                <a
                  href="https://itunes.apple.com/app/apple-store/id548925969?pt=95834802&ct=website&mt=8"
                  target="_blank"
                  rel="noopener noreferrer"
                  className="note-panel__links-item note-panel__links-item--app-store"
                >
                  <img
                    src="/img/appstore.png"
                    srcSet="/img/appstore-md@2x.png 2x"
                    alt=""
                  />
                </a>
                <a
                  href="https://play.google.com/store/apps/details?id=io.wifimap.wifimap&referrer=utm_source%3Dwebsite"
                  target="_blank"
                  rel="noopener noreferrer"
                  className="note-panel__links-item note-panel__links-item--google-play"
                >
                  <img
                    src="/img/googleplay.png"
                    srcSet="/img/googleplay-md@2x.png 2x"
                    alt=""
                  />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <SliderPartners />
  </div>
);
