import gql from 'graphql-tag';

export default gql`
  query fetchTopCities {
    top_cities {
      description
      hotspots_count
      id
      name
      slug
      wikipedia_link
    }
  }
`;
