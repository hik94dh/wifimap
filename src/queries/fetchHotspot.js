import gql from 'graphql-tag';

export const fetchHotspotsCoords = gql`
  query fetchHotspotsCoords($id: ID!, $all_hotspots: String) {
    city(id: $id) {
      id
      hotspots(all_hotspots: $all_hotspots) {
        uuid
        lat
        lng
      }
    }
  }
`;

export const fetchHotspotsList = gql`
  query fetchHotspotsList($id: ID!, $all_hotspots: String) {
    city(id: $id) {
      id
      center_lat
      center_lng
      hotspots_count
      name
      hotspots(all_hotspots: $all_hotspots) {
        address
        download_speed
        uuid
        last_connected_at
        lat
        lng
        name
        ssid
        category
      }
    }
  }
`;

export const fetchHotspot = gql`
  query fetchHotspot($uuid: String, $google_place_id: String) {
    hotspot(uuid: $uuid,  google_place_id: $google_place_id) {
      address
      category
      city {
        slug
      }
      connections_count
      download_speed
      google_place_id
      uuid
      last_connected_at
      lat
      lng
      name
      ping_time
      ssid
      upload_speed
      uuid
      tips {
        id
        created_at
        owner_avatar_url
        owner_id
        owner_name
        password
      }
    }
  }
`;