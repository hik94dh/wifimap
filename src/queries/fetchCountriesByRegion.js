import gql from 'graphql-tag';

export default gql`
  query fetchByRegion($region: String) {
    countries(region: $region) {
      id
      name
      slug
      code
    }
  }
`;
