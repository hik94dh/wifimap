import React from 'react';
import gql  from '../fetchCity';
import gq2  from '../fetchCountriesByRegion';
import gq3  from '../fetchCountry';
import gq4  from '../fetchHotspot';
import gq5  from '../fetchTopCities';
import renderer from 'react-test-renderer';

it('gql  testing', () => {
    const tree = renderer.create(<gql  />).toJSON();
    expect(tree).toMatchSnapshot();
});

it('gq2  testing', () => {
    const tree = renderer.create(<gq2  />).toJSON();
    expect(tree).toMatchSnapshot();
});

it('gq3  testing', () => {
    const tree = renderer.create(<gq3  />).toJSON();
    expect(tree).toMatchSnapshot();
});

it('gq4  testing', () => {
    const tree = renderer.create(<gq4  />).toJSON();
    expect(tree).toMatchSnapshot();
});

it('gq5  testing', () => {
    const tree = renderer.create(<gq5  />).toJSON();
    expect(tree).toMatchSnapshot();
});
