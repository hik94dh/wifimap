import gql from 'graphql-tag';

export default gql`
  query fetchCity($id: ID, $google_place_id: String) {
    city(id: $id, google_place_id: $google_place_id) {
      description
      hotspots_count
      id
      name
      slug
      wikipedia_link
      center_lat
      center_lng
      country {
        name
        slug
      }
      nearby_cities {
        name
        slug
        hotspots_count
      }
    }
  }
`;