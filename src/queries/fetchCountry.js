import gql from 'graphql-tag';

export default gql`
  query fetchCountry($id: ID!) {
    country(id: $id) {
      id
      cities {
        description
        hotspots_count
        id
        name
        slug
        wikipedia_link
      }
      cities_count
      description
      hotspots_count
      id
      name
      region
      slug
      wikipedia_link
    }
  }
`;
