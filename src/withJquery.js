import React, { Component } from 'react';
import $ from 'jquery';

/* eslint-disable */
export default function withJquery(WrappedComponent) {
  return class extends Component {
    componentDidMount() {
      var click = 'click';
      var isMobile = {
        Android: function() {
          return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
          return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
          return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
          return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
          return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
          return (
            isMobile.Android() ||
            isMobile.BlackBerry() ||
            isMobile.iOS() ||
            isMobile.Opera() ||
            isMobile.Windows()
          );
        },
      };
      if (isMobile.any()) {
        $('body').addClass('touch');

        var click = 'touchstart';

        for (
          var sheetI = document.styleSheets.length - 1;
          sheetI >= 0;
          sheetI--
        ) {
          var sheet = document.styleSheets[sheetI];
          // Verify if cssRules exists in sheet
          if (sheet.cssRules) {
            // Loop through each rule in sheet
            for (var ruleI = sheet.cssRules.length - 1; ruleI >= 0; ruleI--) {
              var rule = sheet.cssRules[ruleI];
              // Verify rule has selector text
              if (rule.selectorText) {
                // Replace hover psuedo-class with active psuedo-class
                rule.selectorText = rule.selectorText.replace(
                  ':hover',
                  ':active',
                );
              }
            }
          }
        }
      }

      // page scroll to anchor
      $('.anchor-lnk').on('click', function() {
        var anchor = $(this).data('anchor');
        var anchPos = $(anchor).offset().top;
        $('body,html').animate(
          {
            scrollTop: anchPos,
          },
          500,
        );
      });
    }
    render() {
      return <WrappedComponent {...this.props} />;
    }
  };
}
/* eslint-enable */
