#!/bin/bash

npm run build && aws s3 sync --profile wifimap build/ s3://www.wifimap.io && aws cloudfront create-invalidation --profile wifimap --distribution-id E2U4VTIP8PUCRE --paths "/*"
